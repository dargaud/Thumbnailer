//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------
//
//              File:       WINTOOLS.H
//
//              Purpose:    Header file for WINTOOLS CVI\Win32 instrument driver.
//
//              Assoc.
//              Files:      WINTOOLS.C, WINTOOLS.FP, WINTOOLS.SUB
//
//              Date:       7.26.98
//
//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------

#ifndef WINTOOLS_INCLUDED
#define WINTOOLS_INCLUDED


//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------
// Include required headers 
//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------

#include <cvidef.h>


//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------
// Define driver macros 
//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------

// General values
#define WT_VAL_TRUE                     1          // True! True!
#define WT_VAL_FALSE                    0          // False
#define WT_VAL_MAX_PATH                 260        // Max path length (with NULL)
#define WT_VAL_MAX_PC_NAME              16         // Max machine name length (with NULL)
#define WT_VAL_MAX_ERROR                80         // Maximum error length (with NULL)

// Registry key values
#define WT_KEY_HKCU                     0x80000001 // HKEY_CURRENT_USER
#define WT_KEY_HKCR                     0x80000000 // HKEY_CLASSES_ROOT
#define WT_KEY_HKU                      0x80000003 // HKEY_USERS
#define WT_KEY_HKLM                     0x80000002 // HKEY_LOCAL_MACHINE
#define WT_KEY_HKCC                     0x80000005 // HKEY_CURRENT_CONFIG
#define WT_KEY_HKDD                     0x80000006 // HKEY_DYN_DATA

// System Information values
#define WT_KYBD_IBM_PCXT                1   
#define WT_KYBD_OLIVETTI_ICO            2   
#define WT_KYBD_IBM_PCAT                3   
#define WT_KYBD_IBM_Enh                 4   
#define WT_KYBD_NOKIA_1050              5   
#define WT_KYBD_NOKIA_9140              6   
#define WT_KYBD_JAPANESE                7   
#define WT_PLATFORM_WIN32s              0
#define WT_PLATFORM_WIN95               1 
#define WT_PLATFORM_WINNT               2
#define WT_COLOR_SCROLLBAR              0
#define WT_COLOR_BACKGROUND             1
#define WT_COLOR_ACTIVECAPTION          2
#define WT_COLOR_INACTIVECAPTION        3
#define WT_COLOR_MENU                   4
#define WT_COLOR_WINDOW                 5
#define WT_COLOR_WINDOWFRAME            6
#define WT_COLOR_MENUTEXT               7
#define WT_COLOR_WINDOWTEXT             8
#define WT_COLOR_CAPTIONTEXT            9
#define WT_COLOR_ACTIVEBORDER           10
#define WT_COLOR_INACTIVEBORDER         11
#define WT_COLOR_APPWORKSPACE           12
#define WT_COLOR_HIGHLIGHT              13
#define WT_COLOR_HIGHLIGHTTEXT          14
#define WT_COLOR_BTNFACE                15
#define WT_COLOR_BTNSHADOW              16
#define WT_COLOR_GRAYTEXT               17
#define WT_COLOR_BTNTEXT                18
#define WT_COLOR_INACTIVECAPTIONTEXT    19
#define WT_COLOR_BTNHIGHLIGHT           20

// Message-handling modes
#define WT_MODE_INTERCEPT               1000      // Intercept the message before CVI
#define WT_MODE_IN_QUEUE                1001      // Get the message after CVI

// Taskbar icon menus                  
#define WT_MENU_RIGHT_CLICK             3000      // Popup menu associated with a left-click
#define WT_MENU_LEFT_CLICK              3001      // Popup menu associated with a right-click

// Message-handling attributes
#define WT_ATTR_EXECUTE_MODE            1002      // Type of callback execution

// Generic attributes
#define WT_ATTR_ENABLED                 1003      // Enabled or disabled

// Taskbar status icon attributes
#define WT_ATTR_DIMMED                  5000      // Popup menu item dimmed state
#define WT_ATTR_CHECKED                 5001      // Popup menu item checked state
#define WT_ATTR_DEFAULT_ITEM            5002      // Popup menu default item (bold)
#define WT_ATTR_VISIBLE                 5003      // Popup menu visible
#define WT_ATTR_ICOFILE                 5004      // Tray icon *.ico file
#define WT_ATTR_TOOLTIPTEXT             5005      // Tray Icon ToolTip text
#define WT_ATTR_TOOLTIPTEXT_LENGTH      5006      // Tray Icon ToolTip text length

// Messages
#define WT_EVENT_NEWHANDLE              1004      // New handle due to window recreation
#define WT_EVENT_FILESDROPPED           1005      // Files dropped onto panel
#define WT_EVENT_LEFT_BTN_DOWN          1006      // Left mouse button pressed
#define WT_EVENT_LEFT_BTN_UP            1007      // Left mouse button released
#define WT_EVENT_RIGHT_BTN_DOWN         1008      // Right mouse button pressed
#define WT_EVENT_RIGHT_BTN_UP           1009      // Right mouse button released
#define WT_EVENT_LEFT_BTN_DBL           1010      // Left mouse double-click
#define WT_EVENT_RIGHT_BTN_DBL          1011      // Right mouse double-click
#define WT_EVENT_MENU_ITEM              1012      // Tray Icon popup menu item selected

// General errors
#define WT_ERR_Success                  0
#define WT_ERR_NullPtr                  -1        // Null pointer argument
#define WT_ERR_BufTooSmall              -2        // buffer too small for all data     
#define WT_ERR_CantDynamicLink          -3        // Error occurred during dynamic link
#define WT_ERR_NoOSSupport              -4        // The OS does not support the function
#define WT_ERR_OutOfMemory              -5        // The required memory was not available
#define WT_ERR_UnknownSysError          -6        // Some unexpected condition
#define WT_ERR_InsufficientArgs         -7        // Insufficient number of arguments
#define WT_ERR_InvalidAttrValue         -8        // Invalid value for attribute
#define WT_ERR_InvalidAttribute         -9        // Invalid attribute

// Registry errors
#define WT_ERR_CantOpenRegKey           -10       // Could not open registry key
#define WT_ERR_CantSetRegKeyValue       -11       // Could not set key value
#define WT_ERR_CantAccessKeyValue       -12       // Cant query the key value
#define WT_ERR_RegKeyValueNotExist      -13       // The specified value does not exist for the specified key
#define WT_ERR_WrongKeyValueType        -14       // The specified key value type is not accurate
#define WT_ERR_RegKeyNotExist           -15       // The specified key does not exist

// System information errors
#define WT_ERR_InvalidDispElement       -20       // Display element invalid

// Messaging errors
#define WT_ERR_CallbackNotInstalled     -32       // No handler currently installed
#define WT_ERR_CallbackAlreadyInstalled -33       // Already enabled
#define WT_ERR_NotParentPanel           -34       // Not a parent panel
#define WT_ERR_NoPanelCallback          -35       // No callback for the panel
#define WT_ERR_InvalidPanel             -36       // Not a valid panel
#define WT_ERR_DragDropNotEnabled       -37       // Drag-and-Drop not enabled
#define WT_ERR_InvalidExecuteMode       -38       // Execute mode is not valid

// Taskbar icon errors
#define WT_ERR_NoTrayIconMenu           -50       // Tray icon doesn't have a menu
#define WT_ERR_InvalidTrayMenuItem      -51       // Tray icon menu item invalid
#define WT_ERR_InvalidTrayIcon          -52       // Tray icon invalid
#define WT_ERR_CantLoadIcon             -53       // Cant load icon


//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------
// Define exported driver types 
//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------

// 64-bit integer type
typedef struct {
    unsigned int lo;
    unsigned int hi;
} WT_Int64;

// Message callback function types
typedef int (CVICALLBACK *WinMsgCallback)   (int panelHandle, int message, unsigned int* wParam,
                                             unsigned int* lParam, void* callbackData);   
typedef int (CVICALLBACK *TrayIconCallback) (int iconHandle, int event, int eventData);   


//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------
// Prototype exported driver functions
//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------

// General functions
void CVIFUNC  WT_GetErrorString          (int errorCode, char errorBuffer[]);

// Registry functions
int CVIFUNC   WT_RegWriteBinary          (unsigned int userRootKey, char* userSubKeyName,
                                          char* userValName, char* userData, int userDataSize);
int CVIFUNC   WT_RegReadBinary           (unsigned int userRootKey, char* userSubKeyName,
                                          char* userValName, char* userBuffer, int userBufSize,
                                          int* realDataSize);
int CVIFUNC   WT_RegWriteULong           (unsigned int userRootKey, char* userSubKeyName,
                                          char* userValName, unsigned long userUL, int bigEndian);
int CVIFUNC   WT_RegReadULong            (unsigned int userRootKey, char* userSubKeyName,
                                          char* userValName, unsigned long* userLong,
                                          int bigEndian);
int CVIFUNC   WT_RegWriteString          (unsigned int userRootKey, char* userSubKeyName,
                                          char* userValName, char* userString);
int CVIFUNC   WT_RegReadString           (unsigned int userRootKey, char* userSubKeyName,
                                          char* userValName, char* userString, int userStringSize,
                                          int* realStringSize);

// System information functions
int CVIFUNC   WT_GetSystemColor          (int displayElement, unsigned int* userColor);
int CVIFUNC   WT_GetComputerName         (char nameBuffer[]);
int CVIFUNC   WT_GetWindowsDirs          (char winPathBuffer[], char sysPathBuffer[]);
int CVIFUNC   WT_GetOSVerInfo            (unsigned long* majorVersion, unsigned long* minorVersion,
                                          unsigned long* build, unsigned long* platform);
int CVIFUNC   WT_GetKeyboardParams       (int* keyboardType, int* numFuncKeys,
                                          unsigned long* repeatDelay, unsigned long* speed);
int CVIFUNC   WT_SetKeyboardParams       (unsigned long repeatDelay, unsigned long speed);
int CVIFUNC   WT_GetScreenSaverParams    (int* enabled, int* timeToActivate);
int CVIFUNC   WT_SetScreenSaverParams    (int enabled, int timeToActivate, char* ssFile);
int CVIFUNC   WT_GetDiskSpace            (char* directory, WT_Int64* bytesFreeToCaller,
                                          WT_Int64* totalBytes, WT_Int64* totalBytesFree);
int CVIFUNC   WT_GetMemorySpace          (unsigned long* percentLoad, unsigned long* totalPhysical,
                                          unsigned long* availPhysical, unsigned long* totalPage,
                                          unsigned long* availPage, unsigned long* totalVirtual,
                                          unsigned long* availVirtual); 
int CVIFUNC   WT_SetWallpaper            (int tiled, char* bitmapPath);
int CVIFUNC   WT_GetWallpaper            (int* tiled, char bitmapPath[]);

// User information functions
int CVIFUNC   WT_GetUserName             (char nameBuffer[], int bufferSize, int* realNameSize);

// Windows messaging functions
int CVIFUNC   WT_InstallWinMsgCallback   (int panelHandle, int msgNum, WinMsgCallback pcallback,
                                          int callWhen, void* pcallbackData, int* phwnd);
int CVIFUNC_C WT_SetMsgCallbackAttribute (int panelHandle, int msgNum, int attribute, ...);
int CVIFUNC_C WT_GetMsgCallbackAttribute (int panelHandle, int msgNum, int attribute, ...);
int CVIFUNC   WT_RemoveWinMsgCallback    (int panelHandle, int msgNum);

// Drag and Drop functions
int CVIFUNC   WT_EnableDragAndDrop       (int panelHandle);
int CVIFUNC   WT_DisableDragAndDrop      (int panelHandle);

// Taskbar status icon functions
int CVIFUNC   WT_InstallSysTrayIcon      (char* icoFile, char* toolTipText,
                                          TrayIconCallback taskIconProc, int* htrayIcon);
int CVIFUNC   WT_SetTrayIconAttr         (int hicon, int attribute, char* val);
int CVIFUNC   WT_GetTrayIconAttr         (int huser, int attribute, void* val);
int CVIFUNC   WT_AttachTrayIconMenu      (int iconHandle);
int CVIFUNC   WT_SetTrayIconMenuAttr     (int hicon, int attribute, int val);
int CVIFUNC   WT_GetTrayIconMenuAttr     (int huser, int attribute, int* pval);
int CVIFUNC   WT_InsertTrayIconMenuItem  (int iconHandle, char* itemName, int* pindex);
int CVIFUNC   WT_SetTrayIconMenuItemAttr (int iconHandle, int itemHandle, int attribute, int val);
int CVIFUNC   WT_GetTrayIconMenuItemAttr (int huser, int itemNum, int attribute, int* pval);
int CVIFUNC   WT_DetachTrayIconMenu      (int hicon);
int CVIFUNC   WT_RemoveSysTrayIcon       (int hicon);

#endif // WINTOOLS_INCLUDED
