s��        �   0   	�   �    r                               WT          WinTools                                         � ��WinMsgCallback     � ��TrayIconCallback     	� 	��WT_Int64*     � ��WT_Int64  �    This instrument driver contains functions to simplify the use of many commonly used Windows SDK features.  All functions will dynamically link to any required system DLLs.


Overview of functionality:

  Manipulation of the Windows Registry
  
  Query and control of the operating system for settings,
     resources, and configurations.

  Support for Windows messaging, including Drag and Drop
     notification.

  

  

      �    Functions in this class allow you to communicate with the Windows Registry.

The Windows Registry is a hierarchical database of "Keys", with each Key having associated "Values".  Using functions in this class, you may write and read Value data to and from specified Keys.  The base of the hierarchy is made up by the Root keys:

     HKEY_LOCAL_MACHINE
     HKEY_CLASSES_ROOT 
     HKEY_CURRENT_USER 

Each of these Root keys contains any number of Subkeys, and each Subkey contains any number of Values of specific datatypes.

Applications will typically store "system" settings, such as the installation directory, in Subkeys of HKEY_LOCAL_MACHINE\SOFTWARE, and user-oriented settings, like color schemes, in Subkeys of HKEY_CURRENT_USER\SOFTWARE.

Use Regedit or Regedit32 (on Windows NT) to view the contents of your system Registry.

As an example, CVI stores information in the \SOFTWARE\National Instruments\CVI\5.0 Subkeys of both the HKEY_LOCAL_MACHINE and HKEY_CURRENT_USER Root keys.
       v    Functions in this class allow you to query the OS for information on system settings, configurations, and resources.     q    This class contains functions to query/adjust Windows User-Interface (HW and SW) configurations and properties.     D    This class contains functions to query available system resources.    m    Functions in this class allow you to install a callback for any Windows message.  This functionality is useful for communicating with DLLs or applications that want to notify you of events by posting Windows messages, which they define, to one of your application's windows (CVI panels).  This functionality is not available through CVI's RegisterWinMsgCallback.
     �    This class contains contains functions to enable and disable Drag-And-Drop file notification for CVI panels.  Files may be dragged from Windows Explorer, the Window Shell, or certain Windows applications, and dropped onto CVI panels.     a    Functions in this class allow you to obtain information regarding the currently logged-in user.    �    Functions in this class allow you to install icons in the status area of the desktop status bar (the System Tray).  You may add both left and right-click popup menus to these icons.

System Tray icons are a useful mechanism through which you can allow a user quick-access to your application's features without crowding the taskbar or requiring a window to be open at all times.  Standard procedure dictates the following usage of System Tray icons and their popups:

 - When the user moves the mouse over the icon, the system will display the tooltip that you provided when you added the icon.

- When the user clicks the icon, your application should display a window with additional information.
 
- When the user clicks the icon with the right mouse button, your application should display the shortcut menu.

- When the user double-clicks the icon, your application should execute the default shortcut menu command.      �    Functions in this class allow you to implement popup menus on your System Tray icons.  Standard procedure dictates the following usage of System Tray icon popup menus:

- When the user clicks the icon with the right mouse button, your application should display the shortcut menu.  This shortcut menu should have a default item (indicated in bold type).

- When the user left-clicks or double-left-clicks the icon, your application should execute the default command on the right-click popup menu.       �    This function writes a NULL-terminated ASCII string to the specified Key value in the Windows Registry.  You must specify a Root Key, a Subkey of that Root Key, and the actual Value of that Subkey to which you want to write the data.          The Root Key under which you wish to access a Subkey and its value.  In the Windows Registry, there are several "Root Keys", each of which contains many additional "Subkeys".  In order to communicate with the Windows Registry, you must first specify a Root Key.  


    (    The name of the Subkey (relative to the Root Key), to which you want to write value data.

CVI, for example, stores information in both the \SOFTWARE\National Instruments\CVI\5.0 and \SOFTWARE\National Instruments\CVI\5.0 Subkeys of HKEY_LOCAL_MACHINE and HKEY_CURRENT_USER, respectively.           f    The NULL-terminated ASCII string you wish to write to the specified Value of the specified Subkey.

     8    The name of the value to which you want to write data.    �    The status returned by the function.  0 indicates success, a negative value indicates an error.  Call WT_GetErrorString () for a more detailed description of the error.

Possible values:

   0   WT_ERR_Success 
  -1   WT_ERR_NullPtr
  -2   WT_ERR_BufTooSmall       
  -3   WT_ERR_CantDynamicLink
  -4   WT_ERR_NoOSSupport
  -5   WT_ERR_OutOfMemory
  -6   WT_ERR_UnknownSysError
  -7   WT_ERR_InsufficientArgs
  -8   WT_ERR_InvalidAttrValue
  -9   WT_ERR_InvalidAttribute

 -10   WT_ERR_CantOpenRegKey
 -11   WT_ERR_CantSetRegKeyValue
 -12   WT_ERR_CantAccessKeyValue
 -13   WT_ERR_RegKeyValueNotExist
 -14   WT_ERR_WrongKeyValueType 
 -15   WT_ERR_RegKeyNotExist    , : D     �    Root Key                          @ ; �         Subkey Name                       p � �         String Buffer                     � :b         Value Name                        ���         Status                                        [HKEY_LOCAL_MACHINE WT_KEY_HKLM HKEY_CURRENT_USER WT_KEY_HKCU HKEY_CLASSES_ROOT WT_KEY_HKCR    ""    ""    ""    	               This function reads a NULL-terminated ASCII string from the specified Key Value in the Windows Registry.  You must specify a Root Key, a Subkey of that Root Key, and the actual Value of that Subkey which you want to read.

If you do not know the size of the string in the Registry, you may pass NULL for the "String Buffer" parameter.  The function will determine the size of the string and return it through "Real String Size".  You may then use this value to allocate appropriate space for the string, and call the function again.          The Root Key under which you wish to access a Subkey and its value.  In the Windows Registry, there are several "Root Keys", each of which contains many additional "Subkeys".  In order to communicate with the Windows Registry, you must first specify a Root Key.  


    )    The name of the Subkey (relative to the Root Key), from which you want to read value data.

CVI, for example, stores information in both the \SOFTWARE\National Instruments\CVI\5.0 and \SOFTWARE\National Instruments\CVI\5.0 Subkeys of HKEY_LOCAL_MACHINE and HKEY_CURRENT_USER, respectively.          _    Returns the ASCII NULL-terminated string contents of the specified Value of the specified Subkey.  This buffer must be large enough to receive the entire string.  If you are unsure of the size of the string, you may pass NULL -- the "Real String Size" parameter will return the actual size of the string (including the ASCII NULL) in the Registry.       9    The name of the value from which you want to read data.         The size of the buffer passed to "String Buffer".  This parameter is ignored when you pass NULL in place of an actual buffer.     �    Returns the actual size of the ASCII string associated with the specified registry Key.  You may pass NULL for the "String Buffer" parameter and use this value to allocate adequate memory before calling this function again.     �    The status returned by the function.  0 indicates success, a negative value indicates an error.  Call WT_GetErrorString () for a more detailed description of the error.

Possible values:

   0   WT_ERR_Success 
  -1   WT_ERR_NullPtr
  -2   WT_ERR_BufTooSmall       
  -3   WT_ERR_CantDynamicLink
  -4   WT_ERR_NoOSSupport
  -5   WT_ERR_OutOfMemory
  -6   WT_ERR_UnknownSysError
  -7   WT_ERR_InsufficientArgs
  -8   WT_ERR_InvalidAttrValue
  -9   WT_ERR_InvalidAttribute

 -10   WT_ERR_CantOpenRegKey
 -11   WT_ERR_CantSetRegKeyValue
 -12   WT_ERR_CantAccessKeyValue
 -13   WT_ERR_RegKeyValueNotExist
 -14   WT_ERR_WrongKeyValueType 
 -15   WT_ERR_RegKeyNotExist    o : +     �    Root Key                          � ; �         Subkey Name                       � � S         String Buffer                      :{         Value Name                        \ � �          Buffer Size                       � �{          Real String Size                  �"���         Status                                        [HKEY_LOCAL_MACHINE WT_KEY_HKLM HKEY_CURRENT_USER WT_KEY_HKCU HKEY_CLASSES_ROOT WT_KEY_HKCR    ""    0    ""        	            	            �    This function writes an unsigned long (DWORD) value to the Windows Registry.  You must specify a Root Key, a Subkey of that Root Key, and the actual Value of that Subkey to which you want to write the data.        The Root Key under which you wish to access a Subkey and its value.  In the Windows Registry, there are several "Root Keys", each of which contains many additional "Subkeys".  In order to communicate with the Windows Registry, you must first specify a Root Key.  


    (    The name of the Subkey (relative to the Root Key), to which you want to write value data.

CVI, for example, stores information in both the \SOFTWARE\National Instruments\CVI\5.0 and \SOFTWARE\National Instruments\CVI\5.0 Subkeys of HKEY_LOCAL_MACHINE and HKEY_CURRENT_USER, respectively.           L    The data you wish to write to the specified Value of the specified Subkey.     8    The name of the value to which you want to write data.     �    This parameter controls how the 32-bit integer should be writen to the Registry.  Typically, you will specify Little-Endian (Intel) format.    �    The status returned by the function.  0 indicates success, a negative value indicates an error.  Call WT_GetErrorString () for a more detailed description of the error.

Possible values:

   0   WT_ERR_Success 
  -1   WT_ERR_NullPtr
  -2   WT_ERR_BufTooSmall       
  -3   WT_ERR_CantDynamicLink
  -4   WT_ERR_NoOSSupport
  -5   WT_ERR_OutOfMemory
  -6   WT_ERR_UnknownSysError
  -7   WT_ERR_InsufficientArgs
  -8   WT_ERR_InvalidAttrValue
  -9   WT_ERR_InvalidAttribute

 -10   WT_ERR_CantOpenRegKey
 -11   WT_ERR_CantSetRegKeyValue
 -12   WT_ERR_CantAccessKeyValue
 -13   WT_ERR_RegKeyValueNotExist
 -14   WT_ERR_WrongKeyValueType 
 -15   WT_ERR_RegKeyNotExist    %O : I     �    Root Key                          &c ; �         Subkey Name                       '� � �         Data Value                        '� :g         Value Name                        (' �H          Data Format                       (����         Status                                        [HKEY_LOCAL_MACHINE WT_KEY_HKLM HKEY_CURRENT_USER WT_KEY_HKCU HKEY_CLASSES_ROOT WT_KEY_HKCR    ""    0    ""    Big Endian 1 Little-Endian 0    	            �    This function reads unsigned 32-bit (unsigned long) data from the specified Key Value in the Windows Registry.  You must specify a Root Key, a Subkey of that Root Key, and the actual Value of that Subkey which you want to read.
        The Root Key under which you wish to access a Subkey and its value.  In the Windows Registry, there are several "Root Keys", each of which contains many additional "Subkeys".  In order to communicate with the Windows Registry, you must first specify a Root Key.  


    )    The name of the Subkey (relative to the Root Key), from which you want to read value data.

CVI, for example, stores information in both the \SOFTWARE\National Instruments\CVI\5.0 and \SOFTWARE\National Instruments\CVI\5.0 Subkeys of HKEY_LOCAL_MACHINE and HKEY_CURRENT_USER, respectively.           T    Returns the unsigned long contents of the specified Value of the specified Subkey.     9    The name of the value from which you want to read data.     �    This parameter specifies how the 32-bit integer is stored in the Registry.  Typically, you will specify Little-Endian (Intel) format.    �    The status returned by the function.  0 indicates success, a negative value indicates an error.  Call WT_GetErrorString () for a more detailed description of the error.

Possible values:

   0   WT_ERR_Success 
  -1   WT_ERR_NullPtr
  -2   WT_ERR_BufTooSmall       
  -3   WT_ERR_CantDynamicLink
  -4   WT_ERR_NoOSSupport
  -5   WT_ERR_OutOfMemory
  -6   WT_ERR_UnknownSysError
  -7   WT_ERR_InsufficientArgs
  -8   WT_ERR_InvalidAttrValue
  -9   WT_ERR_InvalidAttribute

 -10   WT_ERR_CantOpenRegKey
 -11   WT_ERR_CantSetRegKeyValue
 -12   WT_ERR_CantAccessKeyValue
 -13   WT_ERR_RegKeyValueNotExist
 -14   WT_ERR_WrongKeyValueType 
 -15   WT_ERR_RegKeyNotExist

    ./ : +     �    Root Key                          /C ; �         Subkey Name                       0t � �         ULong Data                        0� :{         Value Name                        1 �I          Data Format                       1����         Status                                        [HKEY_LOCAL_MACHINE WT_KEY_HKLM HKEY_CURRENT_USER WT_KEY_HKCU HKEY_CLASSES_ROOT WT_KEY_HKCR    ""    	            ""    Big Endian 1 Little-Endian 0    	               This function writes a buffer of data into the Registry in binary format.  You must specify a Root Key, a Subkey of that Root Key, and the actual Value of that Subkey to which you want to write the data.  Any data that exists already at the specified Key value will be overwritten.        The Root Key under which you wish to access a Subkey and its value.  In the Windows Registry, there are several "Root Keys", each of which contains many additional "Subkeys".  In order to communicate with the Windows Registry, you must first specify a Root Key.  


    (    The name of the Subkey (relative to the Root Key), to which you want to write value data.

CVI, for example, stores information in both the \SOFTWARE\National Instruments\CVI\5.0 and \SOFTWARE\National Instruments\CVI\5.0 Subkeys of HKEY_LOCAL_MACHINE and HKEY_CURRENT_USER, respectively.           �    The buffer whose contents you wish to write to the specified Value of the specified Subkey.  This function does not make any assumtions about the contents of this buffer, and data is written as-is to the Registry.

     8    The name of the value to which you want to write data.     �    The number of bytes you wish to copy from the input Buffer into the Registry.  This value must not be any larger than the size of the input Buffer.    �    The status returned by the function.  0 indicates success, a negative value indicates an error.  Call WT_GetErrorString () for a more detailed description of the error.

Possible values:

   0   WT_ERR_Success 
  -1   WT_ERR_NullPtr
  -2   WT_ERR_BufTooSmall       
  -3   WT_ERR_CantDynamicLink
  -4   WT_ERR_NoOSSupport
  -5   WT_ERR_OutOfMemory
  -6   WT_ERR_UnknownSysError
  -7   WT_ERR_InsufficientArgs
  -8   WT_ERR_InvalidAttrValue
  -9   WT_ERR_InvalidAttribute

 -10   WT_ERR_CantOpenRegKey
 -11   WT_ERR_CantSetRegKeyValue
 -12   WT_ERR_CantAccessKeyValue
 -13   WT_ERR_RegKeyValueNotExist
 -14   WT_ERR_WrongKeyValueType 
 -15   WT_ERR_RegKeyNotExist    7Q : I     �    Root Key                          8e ; �         Subkey Name                       9� � �         Data Buffer                       :v :g         Value Name                        :� �/          Bytes to Copy                     ;S!���         Status                                        [HKEY_LOCAL_MACHINE WT_KEY_HKLM HKEY_CURRENT_USER WT_KEY_HKCU HKEY_CLASSES_ROOT WT_KEY_HKCR    ""    ""    ""    0    	           �    This function reads raw binary data from the specified Key Value in the Windows Registry.  You must specify a Root Key, a Subkey of that Root Key, and the actual Value of that Subkey which you want to read.

If you do not know the size of the data in the Registry, you may pass NULL for the "Data Buffer" parameter.  The function will determine the size of the data and return it through "Real Data Size".  You may then use this value to allocate appropriate space for the data, and call the function again.          The Root Key under which you wish to access a Subkey and its value.  In the Windows Registry, there are several "Root Keys", each of which contains many additional "Subkeys".  In order to communicate with the Windows Registry, you must first specify a Root Key.  


    )    The name of the Subkey (relative to the Root Key), from which you want to read value data.

CVI, for example, stores information in both the \SOFTWARE\National Instruments\CVI\5.0 and \SOFTWARE\National Instruments\CVI\5.0 Subkeys of HKEY_LOCAL_MACHINE and HKEY_CURRENT_USER, respectively.          -    Returns the binary contents of the specified Value of the specified Subkey.  This buffer must be large enough to receive all of the binary data.  If you are unsure of the size of the data, you may pass NULL -- the "Real Data Size" parameter will return the actual size of the data in the Registry.       9    The name of the value from which you want to read data.     }    The size of the buffer passed to "Data Buffer".  This parameter is ignored when you pass NULL in place of an actual buffer.     �    Returns the actual size of the data associated with the specified registry Key.  You may pass NULL for the "Data Buffer" parameter and use this value to allocate adequate memory before calling this function again.     �    The status returned by the function.  0 indicates success, a negative value indicates an error.  Call WT_GetErrorString () for a more detailed description of the error.

Possible values:

   0   WT_ERR_Success 
  -1   WT_ERR_NullPtr
  -2   WT_ERR_BufTooSmall       
  -3   WT_ERR_CantDynamicLink
  -4   WT_ERR_NoOSSupport
  -5   WT_ERR_OutOfMemory
  -6   WT_ERR_UnknownSysError
  -7   WT_ERR_InsufficientArgs
  -8   WT_ERR_InvalidAttrValue
  -9   WT_ERR_InvalidAttribute

 -10   WT_ERR_CantOpenRegKey
 -11   WT_ERR_CantSetRegKeyValue
 -12   WT_ERR_CantAccessKeyValue
 -13   WT_ERR_RegKeyValueNotExist
 -14   WT_ERR_WrongKeyValueType 
 -15   WT_ERR_RegKeyNotExist

    A� : +     �    Root Key                          B� ; �         Subkey Name                       D
 � V         Data Buffer                       E? :{         Value Name                        E� � �          Buffer Size                       F �{          Real Data Size                    F����         Status                                        [HKEY_LOCAL_MACHINE WT_KEY_HKLM HKEY_CURRENT_USER WT_KEY_HKCU HKEY_CLASSES_ROOT WT_KEY_HKCR    ""        ""        	            	            V    This function returns detailed information on the current operating system version.
     �    Returns the major version of the operating system.  On Windows NT 4.0, for example, this parameter will contain the integer 4.  You may pass NULL for this parameter.     �    Returns the minor version of the operating system.  On Windows NT 4.0, for example, this parameter will contain the integer 0.  You may pass NULL for this parameter.        Windows NT: Returns the build number of the operating system. 

Windows 95: Returns the build number of the operating system in
            the low-order word. The high-order word contains the
            major and minor version numbers.

You may pass NULL for this parameter.      �    Returns the platform ID of the operating system.  You may pass NULL for this parameter.

Possible values:

  0  WT_PLATFORM_WIN32s 
  1  WT_PLATFORM_WIN95 
  2  WT_PLATFORM_WINNT
    �    The status returned by the function.  0 indicates success, a negative value indicates an error.  Call WT_GetErrorString () for a more detailed description of the error.

Possible values:

   0   WT_ERR_Success 
  -1   WT_ERR_NullPtr
  -2   WT_ERR_BufTooSmall       
  -3   WT_ERR_CantDynamicLink
  -4   WT_ERR_NoOSSupport
  -5   WT_ERR_OutOfMemory
  -6   WT_ERR_UnknownSysError
  -7   WT_ERR_InsufficientArgs
  -8   WT_ERR_InvalidAttrValue
  -9   WT_ERR_InvalidAttribute

 -20   WT_ERR_InvalidDispElement
    K� 7 �          Major Version                     L� | f         Minor Version                     ML | �         Build                             Nk |o         Platform                          O(!���         Status                             	            	            	            	            	            H    This function returns the Windows installation and System directories.     c    Returns the Windows System directory.  If non-NULL, this buffer must be at least WT_VAL_MAX_PATH.    �    The status returned by the function.  0 indicates success, a negative value indicates an error.  Call WT_GetErrorString () for a more detailed description of the error.

Possible values:

   0   WT_ERR_Success 
  -1   WT_ERR_NullPtr
  -2   WT_ERR_BufTooSmall       
  -3   WT_ERR_CantDynamicLink
  -4   WT_ERR_NoOSSupport
  -5   WT_ERR_OutOfMemory
  -6   WT_ERR_UnknownSysError
  -7   WT_ERR_InsufficientArgs
  -8   WT_ERR_InvalidAttrValue
  -9   WT_ERR_InvalidAttribute

 -20   WT_ERR_InvalidDispElement



     i    Returns the Windows installation directory.  If non-NULL, this buffer must be at least WT_VAL_MAX_PATH.    R� NA         System Directory                  S+���         Status                            U1 N �          Windows Directory                  	            	            	            _    This function returns the computer name.  The name will be no longer than WT_VAL_MAX_PC_NAME.     V    Returns the current computer name.  This buffer must be at least WT_VAL_MAX_PC_NAME.    �    The status returned by the function.  0 indicates success, a negative value indicates an error.  Call WT_GetErrorString () for a more detailed description of the error.

Possible values:

   0   WT_ERR_Success 
  -1   WT_ERR_NullPtr
  -2   WT_ERR_BufTooSmall       
  -3   WT_ERR_CantDynamicLink
  -4   WT_ERR_NoOSSupport
  -5   WT_ERR_OutOfMemory
  -6   WT_ERR_UnknownSysError
  -7   WT_ERR_InsufficientArgs
  -8   WT_ERR_InvalidAttrValue
  -9   WT_ERR_InvalidAttribute

 -20   WT_ERR_InvalidDispElement


    V� N �          Computer Name                     W* ���         Status                             	            	               This function retrieves the 0RGB color of a Windows display element.  This is useful if you want to query the OS for its display colors and adjust you own application's display elements, such as Graph plot areaa, which are not affected by the Conform To System Colors attribute.     6    The Windows display element to query for its color.     "    Returns the color of the display element.  The most significant byte of this four-byte value will be 0, and the lower three bytes will contain the Red, Green, and Blue components of the color, repectively.  This corresponds to the color format used throughout the CVI standard libraries.     �    The status returned by the function.  0 indicates success, a negative value indicates an error.  Call WT_GetErrorString () for a more detailed description of the error.

Possible values:

   0   WT_ERR_Success 
  -1   WT_ERR_NullPtr
  -2   WT_ERR_BufTooSmall       
  -3   WT_ERR_CantDynamicLink
  -4   WT_ERR_NoOSSupport
  -5   WT_ERR_OutOfMemory
  -6   WT_ERR_UnknownSysError
  -7   WT_ERR_InsufficientArgs
  -8   WT_ERR_InvalidAttrValue
  -9   WT_ERR_InvalidAttribute

 -20   WT_ERR_InvalidDispElement
    Z� < u          Display Element                   [ 8.         Color                             \9!���         Status                                       �Scrollbar WT_COLOR_SCROLLBAR Background WT_COLOR_BACKGROUND Active Caption WT_COLOR_ACTIVECAPTION Inactive Caption WT_COLOR_INACTIVECAPTION Menu WT_COLOR_MENU Window WT_COLOR_WINDOW Window Frame WT_COLOR_WINDOWFRAME Menu Text WT_COLOR_MENUTEXT Window Text WT_COLOR_WINDOWTEXT Caption Text WT_COLOR_CAPTIONTEXT Active Border WT_COLOR_ACTIVEBORDER Inactive Border WT_COLOR_INACTIVEBORDER Application Workspace WT_COLOR_APPWORKSPACE Highlight WT_COLOR_HIGHLIGHT Highlight Text WT_COLOR_HIGHLIGHTTEXT Button Highlight WT_COLOR_BTNHIGHLIGHT Button Face WT_COLOR_BTNFACE Button Shadow WT_COLOR_BTNSHADOW Gray Text WT_COLOR_GRAYTEXT Button Text WT_COLOR_BTNTEXT Inactive Caption Text WT_COLOR_INACTIVECAPTIONTEXT Button Highlight WT_COLOR_BTNHIGHLIGHT    	           	            J    This function sets the current configuration of the Windows ScreenSaver.     L    This parameter specifies whether or not the ScreenSaver should be enabled.     �    This parameter specifies the time, in seconds, after which the ScreenSaver will activate.  The resolution of this value is 60s.       �    If the "Enabled" parameter is set to true, this parameter specifies the ScreenSaver application (*.scr) path you wish to use.  If the "Enabled" parameter is not set to true, this parameter is ignored and may be NULL.

    �    The status returned by the function.  0 indicates success, a negative value indicates an error.  Call WT_GetErrorString () for a more detailed description of the error.

Possible values:

   0   WT_ERR_Success 
  -1   WT_ERR_NullPtr
  -2   WT_ERR_BufTooSmall       
  -3   WT_ERR_CantDynamicLink
  -4   WT_ERR_NoOSSupport
  -5   WT_ERR_OutOfMemory
  -6   WT_ERR_UnknownSysError
  -7   WT_ERR_InsufficientArgs
  -8   WT_ERR_InvalidAttrValue
  -9   WT_ERR_InvalidAttribute

 -20   WT_ERR_InvalidDispElement
    b= M �           Enabled                           b� U&          Sleep Time                        c � �         ScreenSaver Path                  d  ���         Status                            Yes 1 No 0        ""    	            M    This function obtains the current configuration of the Windows ScreenSaver.     �    Returns a boolean value indicating whether or not the ScreenSaver is currently enabled.  You may pass NULL for this parameter.     �    Returns the time, in seconds, after which the ScreenSaver will activate.  The resolution of this value is 60s.  You may pass NULL for this parameter.    �    The status returned by the function.  0 indicates success, a negative value indicates an error.  Call WT_GetErrorString () for a more detailed description of the error.

Possible values:

   0   WT_ERR_Success 
  -1   WT_ERR_NullPtr
  -2   WT_ERR_BufTooSmall       
  -3   WT_ERR_CantDynamicLink
  -4   WT_ERR_NoOSSupport
  -5   WT_ERR_OutOfMemory
  -6   WT_ERR_UnknownSysError
  -7   WT_ERR_InsufficientArgs
  -8   WT_ERR_InvalidAttrValue
  -9   WT_ERR_InvalidAttribute

 -20   WT_ERR_InvalidDispElement
    gP O �           Enabled                           g� OM          Sleep Time                        hw���         Status                             	            	            	            ;    This function sets the current Windows desktop wallpaper.     �    The pathname of the bitmap (*.BMP) file to set as the Windows desktop wallpaper.  If you do not want any wallpaper, pass an empty string.

Example:

     "C:\\WinNT\\winnt.bmp" or ""     6    The orientation of the bitmap on the desktop window.    �    The status returned by the function.  0 indicates success, a negative value indicates an error.  Call WT_GetErrorString () for a more detailed description of the error.

Possible values:

   0   WT_ERR_Success 
  -1   WT_ERR_NullPtr
  -2   WT_ERR_BufTooSmall       
  -3   WT_ERR_CantDynamicLink
  -4   WT_ERR_NoOSSupport
  -5   WT_ERR_OutOfMemory
  -6   WT_ERR_UnknownSysError
  -7   WT_ERR_InsufficientArgs
  -8   WT_ERR_InvalidAttrValue
  -9   WT_ERR_InvalidAttribute

 -20   WT_ERR_InvalidDispElement
    k� Y<         Bitmap File                       l@ T �           Display Style                     l~ ���         Status                             ""    Tiled 1 Centered 0    	            �    This function gets the current Windows desktop wallpaper.  This is useful if your application is going to change the wallpaper, as it must be able to restore the original settings when it completes.        A buffer into which the function will copy the full NULL-terminated pathname of the desktop wallpaper file (a bitmap).  This buffer must be at least WT_VAL_MAX_PATH bytes.  If no wallpaper is currently configured, the function will return an empty string.
     �    Returns a boolean indicating whether or not the walpaper is tiled.  If this value is 0, then the wallpaper is centered on the desktop.     �    The status returned by the function.  0 indicates success, a negative value indicates an error.  Call WT_GetErrorString () for a more detailed description of the error.

Possible values:

   0   WT_ERR_Success 
  -1   WT_ERR_NullPtr
  -2   WT_ERR_BufTooSmall       
  -3   WT_ERR_CantDynamicLink
  -4   WT_ERR_NoOSSupport
  -5   WT_ERR_OutOfMemory
  -6   WT_ERR_UnknownSysError
  -7   WT_ERR_InsufficientArgs
  -8   WT_ERR_InvalidAttrValue
  -9   WT_ERR_InvalidAttribute

 -20   WT_ERR_InvalidDispElement
    p Y<         Bitmap File                       q" T �           Tiled                             q����         Status                             	            	            	            X    This function gets the type and timing attributes of the currently installed keyboard.     �    Returns an integer containing the speed of the currently installed keyboard (0-31 increasing).  You may pass NULL for this parameter.     �    Returns an integer indicating the repeat delay of the currently installed keyboard (0-3 increasing).  You may pass NULL for this parameter.    g    Returns an integer indicating the type of currently installed keyboard.  You may pass NULL for this parameter.

Possible output values:

  1  WT_KYBD_IBM_PCXT         
  2  WT_KYBD_OLIVETTI_ICO     
  3  WT_KYBD_IBM_PCAT          
  4  WT_KYBD_IBM_Enh          
  5  WT_KYBD_NOKIA_1050       
  6  WT_KYBD_NOKIA_9140       
  7  WT_KYBD_JAPANESE         


     �    Returns an integer indicating the number of function keys on the currently installed keyboard.  You may pass NULL for this parameter.    �    The status returned by the function.  0 indicates success, a negative value indicates an error.  Call WT_GetErrorString () for a more detailed description of the error.

Possible values:

   0   WT_ERR_Success 
  -1   WT_ERR_NullPtr
  -2   WT_ERR_BufTooSmall       
  -3   WT_ERR_CantDynamicLink
  -4   WT_ERR_NoOSSupport
  -5   WT_ERR_OutOfMemory
  -6   WT_ERR_UnknownSysError
  -7   WT_ERR_InsufficientArgs
  -8   WT_ERR_InvalidAttrValue
  -9   WT_ERR_InvalidAttribute

 -20   WT_ERR_InvalidDispElement
    t� �2         Speed                             uh � �         Repeat Delay                      u� = �           Keyboard Type                     wl >2          Num Function Keys                 w����         Status                             	            	            	            	            	            O    This function sets the timing attributes of the currently installed keyboard.     @    This parameter specifies the keyboard speed (0-31 increasing).     F    This parameter specifies the keyboard repeat delay (0-3 increasing).    �    The status returned by the function.  0 indicates success, a negative value indicates an error.  Call WT_GetErrorString () for a more detailed description of the error.

Possible values:

   0   WT_ERR_Success 
  -1   WT_ERR_NullPtr
  -2   WT_ERR_BufTooSmall       
  -3   WT_ERR_CantDynamicLink
  -4   WT_ERR_NoOSSupport
  -5   WT_ERR_OutOfMemory
  -6   WT_ERR_UnknownSysError
  -7   WT_ERR_InsufficientArgs
  -8   WT_ERR_InvalidAttrValue
  -9   WT_ERR_InvalidAttribute

 -20   WT_ERR_InvalidDispElement
    {� W-         Speed                             {� W �          Repeat Delay                      |0���         Status                             31    1    	            K    This function determines the free space in a specified directory on disk.     �    The directory for which to obtain size information.  This paramater can be a root drive, or any subdirectory off of that drive.  This function will obtain the space available within this directory (including subdirectories).    �    Returns the total number of bytes in the specified directory free to the calling process.  Depending on OS settings, this value may be less than that returned in "Total Bytes Free".

This 64-bit integer value is returned in the following structure:

    typedef struct {
        unsigned int lo;
        unsigned int hi;
    } WT_Int64;

The "lo" member will contain the low-order 32-bits of the value, and the "hi" member will contain the high-order 32-bits of the value.    b    Returns the total number of bytes in the specified directory.

This 64-bit integer value is returned in the following structure:

    typedef struct {
        unsigned int lo;
        unsigned int hi;
    } WT_Int64;

The "lo" member will contain the low-order 32-bits of the value, and the "hi" member will contain the high-order 32-bits of the value.    �    Returns the total number of free bytes in the specified directory.  You may pass NULL for this parameter. 

The 64-bit integer value is returned in the following structure:

    typedef struct {
        unsigned int lo;
        unsigned int hi;
    } WT_Int64;

The "lo" member will contain the low-order 32-bits of the value, and the "hi" member will contain the high-order 32-bits of the value.    �    The status returned by the function.  0 indicates success, a negative value indicates an error.  Call WT_GetErrorString () for a more detailed description of the error.

Possible values:

   0   WT_ERR_Success 
  -1   WT_ERR_NullPtr
  -2   WT_ERR_BufTooSmall       
  -3   WT_ERR_CantDynamicLink
  -4   WT_ERR_NoOSSupport
  -5   WT_ERR_OutOfMemory
  -6   WT_ERR_UnknownSysError
  -7   WT_ERR_InsufficientArgs
  -8   WT_ERR_InvalidAttrValue
  -9   WT_ERR_InvalidAttribute

 -20   WT_ERR_InvalidDispElement


    < P /          Directory                         �& P � �       Bytes Free to Caller              � P! �       Total Bytes                       �r P� �       Total Bytes Free                ����  ?��                                           ����         Status                             "C:"    	            	            	            RThis function is only supported on Windows 95 OSR2 and Windows NT Workstation 4.0    	            �    This function determines the size of the system's physical, Page File, and Virtual memory, as well as the portions of each which are availbe to the calling process.     T    Returns the load on available memory as a percentage.  This paramater may be NULL.     Q    Returns the total physical system memory in bytes.  This paramater may be NULL.     R    Returns the total system Page File memory in bytes.  This paramater may be NULL.     P    Returns the total virtual system memory in bytes.  This paramater may be NULL.     U    Returns the available physical system memory in bytes.  This paramater may be NULL.     M    Returns the available Page File size in bytes.  This paramater may be NULL.     T    Returns the available virtual system memory in bytes.  This paramater may be NULL.    �    The status returned by the function.  0 indicates success, a negative value indicates an error.  Call WT_GetErrorString () for a more detailed description of the error.

Possible values:

   0   WT_ERR_Success 
  -1   WT_ERR_NullPtr
  -2   WT_ERR_BufTooSmall       
  -3   WT_ERR_CantDynamicLink
  -4   WT_ERR_NoOSSupport
  -5   WT_ERR_OutOfMemory
  -6   WT_ERR_UnknownSysError
  -7   WT_ERR_InsufficientArgs
  -8   WT_ERR_InvalidAttrValue
  -9   WT_ERR_InvalidAttribute

 -20   WT_ERR_InvalidDispElement
    �� # �          Percent Load                      �� h f         Total Physical                    �; h �         Total Page File                   �� ho         Total Virtual                     �� � g         Available Physical                �J � �         Available Page File               �� �o         Available Virtual                 ��!���         Status                             	            	            	            	            	            	            	            	           O    This function installs a callback function for a specific Windows message posted to a CVI panel.  A normal ProcessSystemEvents () or RunUserInterface () call will cause the panel to receive the message, and the specified callback will be passed the message data.

NOTE:  A message callback cannot be installed for a CVI Child panel.      �    The number of the message for which you would like to install a callback function.

NOTE: You may not respond to WM_DISCARD or WM_DROPFILES messages.        �    The callback function to be associated with the specified Windows message.  The driver will call this function when the message handler detects the specified message.  It will pass all Windows message data to the function.  

The function must be of the prototype:

     int CVICALLBACK MyCallback (int message, int* wParam,
                                 int* lParam, 
                                 void* callbackData);

If your callback is configured to intercept the message (execution mode = WT_MODE_INTERCEPT), the function should return 0 if you want the message to be passed on to CVI's normal panel message-handler.  If the function returns a 1, then the message will not be detected by CVI's message-handler.  Note that the function has access to message data pointers.  If passing the messages on to CVI's message handler, you may alter the contents of these pointers and CVI's message-handler will receive the "adjusted" message data.

An event WT_EVENT_NEWHANDLE may be passed to your callback through the "message" parameter.  When this occurs, it is because the Windows handle of the CVI panel has changed internally.  The new Windows handle, which you must now use to post messages, will be passed in via the contents of the wParam pointer.

NOTE:  If configured for "Intercept Mode" (WT_MODE_INTERCEPT), any breakpoints inside the callback will not be honored.

NOTE:  You may not install a message callback on a CVI Child panel.    �    The status returned by the function.  0 indicates success, a negative value indicates an error.  Call WT_GetErrorString () for a more detailed description of the error.

Possible values:

   0   WT_ERR_Success 
  -1   WT_ERR_NullPtr
  -2   WT_ERR_BufTooSmall       
  -3   WT_ERR_CantDynamicLink
  -4   WT_ERR_NoOSSupport
  -5   WT_ERR_OutOfMemory
  -6   WT_ERR_UnknownSysError
  -7   WT_ERR_InsufficientArgs
  -8   WT_ERR_InvalidAttrValue
  -9   WT_ERR_InvalidAttribute

  -32  WT_ERR_CallbackNotInstalled
  -33  WT_ERR_CallbackAlreadyInstalled
  -34  WT_ERR_NotParentPanel
  -35  WT_ERR_NoPanelCallback
  -36  WT_ERR_InvalidPanel
  -37  WT_ERR_DragDropNotEnabled
  -38  WT_ERR_InvalidExecuteMode     `    The handle of the CVI panel for which you wish to install a Windows message callback function.    p    This parameter controls when your callback will actually execute.  

In In-Queue Mode, CVI's handler sees the message and a call to your callback is placed in CVI's standard UI event queue.

In Intercept Mode, the callback is executed before CVI sees the message.  You would use this mode if you needed to adjust or swallow messages before CVI's handler  sees them.
     j    A pointer to be passed to your message callback function, similar to the standard UI callback mechanism.    5    The Window Handle that should be used to post a message to this CVI panel.  On certain conditions, this handle may change internally.  Therefore, it is necessary to respond to the WT_EVENT_NEWHANDLE message in your callback function and then use the new handle, passed via the contents of the wParam input.
    �] E �          Message Number                    �  E{ �       Callback Function                 �����         Status                            �w E T           Panel Handle                      �� � {   �      Calllback Mode                    �W � �         Callback Data                     �� �t          Posting Handle                             	              6 In-Queue WT_MODE_IN_QUEUE Intercept WT_MODE_INTERCEPT    NULL    	            m    Sets an attribute of the callback mechanism linked to the specified message number and the specified panel.     Z    The number of the message for whose callback mechanism you wish to set an attribute.         J    The attribute you would like to set for the specified callback function.     /    The desired value of the specified attribute.    �    The status returned by the function.  0 indicates success, a negative value indicates an error.  Call WT_GetErrorString () for a more detailed description of the error.

Possible values:

   0   WT_ERR_Success 
  -1   WT_ERR_NullPtr
  -2   WT_ERR_BufTooSmall       
  -3   WT_ERR_CantDynamicLink
  -4   WT_ERR_NoOSSupport
  -5   WT_ERR_OutOfMemory
  -6   WT_ERR_UnknownSysError
  -7   WT_ERR_InsufficientArgs
  -8   WT_ERR_InvalidAttrValue
  -9   WT_ERR_InvalidAttribute

  -32  WT_ERR_CallbackNotInstalled
  -33  WT_ERR_CallbackAlreadyInstalled
  -34  WT_ERR_NotParentPanel
  -35  WT_ERR_NoPanelCallback
  -36  WT_ERR_InvalidPanel
  -37  WT_ERR_DragDropNotEnabled
  -38  WT_ERR_InvalidExecuteMode     D    The handle of the panel for which a message-callback is installed.    �S ; �          Message Number                    �� ;]         Callback Attribute                � � �         Attribute Value                   �>���         Status                            � ; Z           Panel Handle                                       0        	                m    Gets an attribute of the callback mechanism linked to the specified message number and the specified panel.     Z    The number of the message for whose callback mechanism you wish to get an attribute.         J    The attribute you would like to set for the specified callback function.     /    The current value of the specified attribute.    �    The status returned by the function.  0 indicates success, a negative value indicates an error.  Call WT_GetErrorString () for a more detailed description of the error.

Possible values:

   0   WT_ERR_Success 
  -1   WT_ERR_NullPtr
  -2   WT_ERR_BufTooSmall       
  -3   WT_ERR_CantDynamicLink
  -4   WT_ERR_NoOSSupport
  -5   WT_ERR_OutOfMemory
  -6   WT_ERR_UnknownSysError
  -7   WT_ERR_InsufficientArgs
  -8   WT_ERR_InvalidAttrValue
  -9   WT_ERR_InvalidAttribute

  -32  WT_ERR_CallbackNotInstalled
  -33  WT_ERR_CallbackAlreadyInstalled
  -34  WT_ERR_NotParentPanel
  -35  WT_ERR_NoPanelCallback
  -36  WT_ERR_InvalidPanel
  -37  WT_ERR_DragDropNotEnabled
  -38  WT_ERR_InvalidExecuteMode     D    The handle of the panel for which a message-callback is installed.    �� B �          Message Number                    �W B^         Callback Attribute                �� � �          Attribute Value                   �����         Status                            �� B \           Panel Handle                                       0    	            	                t    This function detaches the callback function for a specific panel and message number, and frees associated memory.    �    The status returned by the function.  0 indicates success, a negative value indicates an error.  Call WT_GetErrorString () for a more detailed description of the error.

Possible values:

   0   WT_ERR_Success 
  -1   WT_ERR_NullPtr
  -2   WT_ERR_BufTooSmall       
  -3   WT_ERR_CantDynamicLink
  -4   WT_ERR_NoOSSupport
  -5   WT_ERR_OutOfMemory
  -6   WT_ERR_UnknownSysError
  -7   WT_ERR_InsufficientArgs
  -8   WT_ERR_InvalidAttrValue
  -9   WT_ERR_InvalidAttribute

  -32  WT_ERR_CallbackNotInstalled
  -33  WT_ERR_CallbackAlreadyInstalled
  -34  WT_ERR_NotParentPanel
  -35  WT_ERR_NoPanelCallback
  -36  WT_ERR_InvalidPanel
  -37  WT_ERR_DragDropNotEnabled
  -38  WT_ERR_InvalidExecuteMode     N    The handle of the CVI panel for which a message-callback should be detached.     _    The number of the message for which you would like to detach an installed callback function.
    �����         Status                            �i B �           Panel Handle                      �� B2          Message Number                     	                   u    This function enables Drag-And-Drop notification for the specified panel.  After calling this function, the panel's standard callback function will be passed an event WT_EVENT_FILESDROPPED whenever files are dropped onto the panel.  

The eventData1 parameter will be a pointer to a block of memory indicating the number of files dropped, and the full path names.  The first four bytes of the block will be an integer representing the number of files dropped (n).  Starting at the fifth byte will be a series of WT_VAL_MAX_PATH byte blocks containing NULL-terminated strings indicating each file name.  Thus, the total size of the passed buffer will be:
       
        (4 + n * WT_VAL_MAX_PATH) bytes

The eventData2 parameter will be a pointer to a CVI Point structure indicating the coordinates at which the files were dropped, relative to the top-left of the panel.

NOTE:  Any breakpoints in the panel callback function will not be honored when passed the WT_EVENT_DROPFILES event.

NOTE:  This function should not be called for a CVI Child panel; Child panels will respond to Drag-And-Drop notifications as long as their Parent does.     U    The handle of the CVI panel for which Drag-And-Drop notification should be enabled.    �    The status returned by the function.  0 indicates success, a negative value indicates an error.  Call WT_GetErrorString () for a more detailed description of the error.

Possible values:

   0   WT_ERR_Success 
  -1   WT_ERR_NullPtr
  -2   WT_ERR_BufTooSmall       
  -3   WT_ERR_CantDynamicLink
  -4   WT_ERR_NoOSSupport
  -5   WT_ERR_OutOfMemory
  -6   WT_ERR_UnknownSysError
  -7   WT_ERR_InsufficientArgs
  -8   WT_ERR_InvalidAttrValue
  -9   WT_ERR_InvalidAttribute

  -32  WT_ERR_CallbackNotInstalled
  -33  WT_ERR_CallbackAlreadyInstalled
  -34  WT_ERR_NotParentPanel
  -35  WT_ERR_NoPanelCallback
  -36  WT_ERR_InvalidPanel
  -37  WT_ERR_DragDropNotEnabled
  -38  WT_ERR_InvalidExecuteMode    �V K �           Panel Handle                      �����         Status                                 	            M    This function disables Drag-And-Drop notification for the specified panel.      V    The handle of the CVI panel for which Drag-And-Drop notification should be disabled.    �    The status returned by the function.  0 indicates success, a negative value indicates an error.  Call WT_GetErrorString () for a more detailed description of the error.

Possible values:

   0   WT_ERR_Success 
  -1   WT_ERR_NullPtr
  -2   WT_ERR_BufTooSmall       
  -3   WT_ERR_CantDynamicLink
  -4   WT_ERR_NoOSSupport
  -5   WT_ERR_OutOfMemory
  -6   WT_ERR_UnknownSysError
  -7   WT_ERR_InsufficientArgs
  -8   WT_ERR_InvalidAttrValue
  -9   WT_ERR_InvalidAttribute

  -32  WT_ERR_CallbackNotInstalled
  -33  WT_ERR_CallbackAlreadyInstalled
  -34  WT_ERR_NotParentPanel
  -35  WT_ERR_NoPanelCallback
  -36  WT_ERR_InvalidPanel
  -37  WT_ERR_DragDropNotEnabled
  -38  WT_ERR_InvalidExecuteMode    �E @ �           Panel Handle                      �����         Status                                 	            A    This function returns the name of the currently logged-in user.    '    Returns the name of the currently logged-in user.  This buffer must be large enough to hold the complete name.  If you are unsure of the size of the name, you may safely pass NULL in this parameter and the function will return the required buffer size through the "Actual Name Size" parameter.     �    The size of the buffer passed to the "User Name" parameter.  If you are passing NULL into that parameter, then this value is ignored.     �    Returns the exact size of the logged-in user's name.  The buffer you pass to the "User Name" parameter must be at least this large (or NULL).    �    The status returned by the function.  0 indicates success, a negative value indicates an error.  Call WT_GetErrorString () for a more detailed description of the error.

Possible values:

   0   WT_ERR_Success 
  -1   WT_ERR_NullPtr
  -2   WT_ERR_BufTooSmall       
  -3   WT_ERR_CantDynamicLink
  -4   WT_ERR_NoOSSupport
  -5   WT_ERR_OutOfMemory
  -6   WT_ERR_UnknownSysError
  -7   WT_ERR_InsufficientArgs
  -8   WT_ERR_InvalidAttrValue
  -9   WT_ERR_InvalidAttribute
    �) L [          User Name                         �X T �          Buffer Size                       �� Lh          Actual Name Size                  �~���         Status                             	            0    	            	           #    This function installs a user-defined icon in the Status area of the Windows Desktop Taskbar (the System Tray).  The function specified in the Callback Function parameter will receive mouse events from the icon.

Use WT_CreateTrayIconMenu () to attach popup menus to your System Tray icon.     X    A NULL-terminated string indicating the *.ico file to be displayed in the System Tray.        A NULL-temrinated string containing the text to be displayed when a user moves the mouse over the System Tray icon.  The popup message (a ToolTip) will be displayed in the default system colors.  The system will honor a maximum of 64 characters for this string.    m    The callback function to receive events from the System Tray icon.  The function should be of the following form:

    int CVICALLBACK Func (int iconHandle, int event,
                          int eventData);   

When an event occurs on the icon, CVI will call this function.  The iconHandle parameter will contain a handle to the icon which generated the event -- this handle will have been returned from WT_InstallSysTrayIcon ().  The event parameter will be one of the following values:

    WT_EVENT_LEFT_BTN_DOWN          
    WT_EVENT_LEFT_BTN_UP            
    WT_EVENT_RIGHT_BTN_DOWN         
    WT_EVENT_RIGHT_BTN_UP           
    WT_EVENT_LEFT_BTN_DBL           
    WT_EVENT_RIGHT_BTN_DBL          
    WT_EVENT_MENU_ITEM

The eventData parameter will be 0 for all events other than WT_EVENT_MENU_ITEM. You will only receive WT_EVENT_MENU_ITEM events if you install a popup menu for this icon.  In the case of this event, eventData will contain the ID of the menu item selected.

The callback function should return 0 unless you want to prevent an installed popup menu from appearing (return non-zero in this case).
    Q    The status returned by the function.  0 indicates success, a negative value indicates an error.  Call WT_GetErrorString () for a more detailed description of the error.

Possible values:

   0   WT_ERR_Success 
  -1   WT_ERR_NullPtr
  -2   WT_ERR_BufTooSmall       
  -3   WT_ERR_CantDynamicLink
  -4   WT_ERR_NoOSSupport
  -5   WT_ERR_OutOfMemory
  -6   WT_ERR_UnknownSysError
  -7   WT_ERR_InsufficientArgs
  -8   WT_ERR_InvalidAttrValue
  -9   WT_ERR_InvalidAttribute

  -50  WT_ERR_NoTrayIconMenu
  -51  WT_ERR_InvalidTrayMenuItem
  -52  WT_ERR_InvalidTrayIcon
  -53  WT_ERR_CantLoadIcon     +    Returns a handle to the System tray icon.     9 S          Icon Image File                   �� : �         ToolTip Text                      �� ;q �       Callback Function                 �k���         Status                            �� � �          Icon Handle                        ""    ""        	            	            �    This function removes the specied icon from the System Tray and frees all resources.  The function destroys any menus associated with the icon.
    Q    The status returned by the function.  0 indicates success, a negative value indicates an error.  Call WT_GetErrorString () for a more detailed description of the error.

Possible values:

   0   WT_ERR_Success 
  -1   WT_ERR_NullPtr
  -2   WT_ERR_BufTooSmall       
  -3   WT_ERR_CantDynamicLink
  -4   WT_ERR_NoOSSupport
  -5   WT_ERR_OutOfMemory
  -6   WT_ERR_UnknownSysError
  -7   WT_ERR_InsufficientArgs
  -8   WT_ERR_InvalidAttrValue
  -9   WT_ERR_InvalidAttribute

  -50  WT_ERR_NoTrayIconMenu
  -51  WT_ERR_InvalidTrayMenuItem
  -52  WT_ERR_InvalidTrayIcon
  -53  WT_ERR_CantLoadIcon     p    The handle of the System Tray icon to remove.  This handle will have been returned from WT_InstallSysTrayIcon.    �����         Status                            � I �           Icon Handle                        	                �    This function sets an attribute of the specified System Tray icon.

See the Class Help for more information on the recommended usage of System Tray icons and their popup menus.
    Q    The status returned by the function.  0 indicates success, a negative value indicates an error.  Call WT_GetErrorString () for a more detailed description of the error.

Possible values:

   0   WT_ERR_Success 
  -1   WT_ERR_NullPtr
  -2   WT_ERR_BufTooSmall       
  -3   WT_ERR_CantDynamicLink
  -4   WT_ERR_NoOSSupport
  -5   WT_ERR_OutOfMemory
  -6   WT_ERR_UnknownSysError
  -7   WT_ERR_InsufficientArgs
  -8   WT_ERR_InvalidAttrValue
  -9   WT_ERR_InvalidAttribute

  -50  WT_ERR_NoTrayIconMenu
  -51  WT_ERR_InvalidTrayMenuItem
  -52  WT_ERR_InvalidTrayIcon
  -53  WT_ERR_CantLoadIcon     �    The handle of the System Tray icon for which you wish to set an  attribute.  This handle will have been returned from WT_InstallSysTrayIcon.     1    The desired value of the specified attribute.

     *    The tray icon attribute you wish to set.    �����         Status                            �! I \           Icon Handle                       ӷ Hr          Value                             �� H �         Tray Icon Attribute                	                                0    �    This function gets the current value of an attribute of the specified System Tray icon.

See the Class Help for more information on the recommended usage of System Tray icons and their popup menus.
    Q    The status returned by the function.  0 indicates success, a negative value indicates an error.  Call WT_GetErrorString () for a more detailed description of the error.

Possible values:

   0   WT_ERR_Success 
  -1   WT_ERR_NullPtr
  -2   WT_ERR_BufTooSmall       
  -3   WT_ERR_CantDynamicLink
  -4   WT_ERR_NoOSSupport
  -5   WT_ERR_OutOfMemory
  -6   WT_ERR_UnknownSysError
  -7   WT_ERR_InsufficientArgs
  -8   WT_ERR_InvalidAttrValue
  -9   WT_ERR_InvalidAttribute

  -50  WT_ERR_NoTrayIconMenu
  -51  WT_ERR_InvalidTrayMenuItem
  -52  WT_ERR_InvalidTrayIcon
  -53  WT_ERR_CantLoadIcon     �    The handle of the System Tray icon for which you wish to get an  attribute value.  This handle will have been returned from WT_InstallSysTrayIcon.     9    Returns the current value of the specified attribute.

     6    The tray icon attribute whose value you wish to get.    �����         Status                            �E I \           Icon Handle                       �� Hr         Value                             �" H �         Tray Icon Attribute                	                	                        0       This function attaches a right-click popup menu to the specified System Tray icon.  See the Class Help for more information on the recommended usage of System Tray icons and their popup menus.

You will add items to this popup menu with WT_InsertTrayIconMenuItem ().
    Q    The status returned by the function.  0 indicates success, a negative value indicates an error.  Call WT_GetErrorString () for a more detailed description of the error.

Possible values:

   0   WT_ERR_Success 
  -1   WT_ERR_NullPtr
  -2   WT_ERR_BufTooSmall       
  -3   WT_ERR_CantDynamicLink
  -4   WT_ERR_NoOSSupport
  -5   WT_ERR_OutOfMemory
  -6   WT_ERR_UnknownSysError
  -7   WT_ERR_InsufficientArgs
  -8   WT_ERR_InvalidAttrValue
  -9   WT_ERR_InvalidAttribute

  -50  WT_ERR_NoTrayIconMenu
  -51  WT_ERR_InvalidTrayMenuItem
  -52  WT_ERR_InvalidTrayIcon
  -53  WT_ERR_CantLoadIcon     �    The handle of the System Tray icon for which you wish to attach a popup menu.  This handle will have been returned from WT_InstallSysTrayIcon ().    �w���         Status                            �� S �           Icon Handle                        	                �    This function sets an attribute of the specified System Tray icon's popup menu.

See the Class Help for more information on the recommended usage of System Tray icons and their popup menus.
    Q    The status returned by the function.  0 indicates success, a negative value indicates an error.  Call WT_GetErrorString () for a more detailed description of the error.

Possible values:

   0   WT_ERR_Success 
  -1   WT_ERR_NullPtr
  -2   WT_ERR_BufTooSmall       
  -3   WT_ERR_CantDynamicLink
  -4   WT_ERR_NoOSSupport
  -5   WT_ERR_OutOfMemory
  -6   WT_ERR_UnknownSysError
  -7   WT_ERR_InsufficientArgs
  -8   WT_ERR_InvalidAttrValue
  -9   WT_ERR_InvalidAttribute

  -50  WT_ERR_NoTrayIconMenu
  -51  WT_ERR_InvalidTrayMenuItem
  -52  WT_ERR_InvalidTrayIcon
  -53  WT_ERR_CantLoadIcon     �    The handle of the System Tray icon for which you wish to set a  popup menu attribute.  This handle will have been returned from WT_InstallSysTrayIcon ().     1    The desired value of the specified attribute.

     +    The popup menu attribute you wish to set.    ߭���         Status                            � I �           Icon Handle                       � � �          Value                             �� H$         Popup Menu Attribute               	                                0    �    This function gets the value of an attribute of the specified System Tray icon's popup menu.

See the Class Help for more information on the recommended usage of System Tray icons and their popup menus.
    Q    The status returned by the function.  0 indicates success, a negative value indicates an error.  Call WT_GetErrorString () for a more detailed description of the error.

Possible values:

   0   WT_ERR_Success 
  -1   WT_ERR_NullPtr
  -2   WT_ERR_BufTooSmall       
  -3   WT_ERR_CantDynamicLink
  -4   WT_ERR_NoOSSupport
  -5   WT_ERR_OutOfMemory
  -6   WT_ERR_UnknownSysError
  -7   WT_ERR_InsufficientArgs
  -8   WT_ERR_InvalidAttrValue
  -9   WT_ERR_InvalidAttribute

  -50  WT_ERR_NoTrayIconMenu
  -51  WT_ERR_InvalidTrayMenuItem
  -52  WT_ERR_InvalidTrayIcon
  -53  WT_ERR_CantLoadIcon     �    The handle of the System Tray icon for which you wish to get a  popup menu attribute.  This handle will have been returned from WT_InstallSysTrayIcon ().     1    The current value of the specified attribute.

     7    The popup menu attribute whose value you wish to get.    �����         Status                            �= I �           Icon Handle                       �� � �          Value                             � H$         Popup Menu Attribute               	                	                       :Default Item WT_ATTR_DEFAULT_ITEM Visible WT_ATTR_VISIBLE   �    This function adds an item to the specified icon's right-click popup menu. Items are added in order, with the first item being item 1, and grow vertically on the popup menu.  

The Callback Function specified in WT_InstallSysTrayIcon () will receive WT_EVENT_MENU_ITEM events when the user selects a menu item -- eventData will contain the 1-based index of the selected item.  The callback function will receive WT_EVENT_RIGHT_BUTTON_DOWN events before any associated popups are diplayed.  If you return non-zero from the callback function upon receiving these events, the popup menu will not be displayed.   

See the Class Help for more information on the recommended usage of System Tray icons and their popup menus.
    Q    The status returned by the function.  0 indicates success, a negative value indicates an error.  Call WT_GetErrorString () for a more detailed description of the error.

Possible values:

   0   WT_ERR_Success 
  -1   WT_ERR_NullPtr
  -2   WT_ERR_BufTooSmall       
  -3   WT_ERR_CantDynamicLink
  -4   WT_ERR_NoOSSupport
  -5   WT_ERR_OutOfMemory
  -6   WT_ERR_UnknownSysError
  -7   WT_ERR_InsufficientArgs
  -8   WT_ERR_InvalidAttrValue
  -9   WT_ERR_InvalidAttribute

  -50  WT_ERR_NoTrayIconMenu
  -51  WT_ERR_InvalidTrayMenuItem
  -52  WT_ERR_InvalidTrayIcon
  -53  WT_ERR_CantLoadIcon     �    The handle of the System Tray icon for which you wish to add a a popup menu item.  This handle will have been returned from WT_InstallSysTrayIcon ().     �    A NULL-terminated string indicating the name of the popup menu item.  This is the name which will appear on the popup menu.  If you pass NULL, then the created item will be a separator, for which you will not receive events.     1    The 1-based index of the newly inserted item.      �k���         Status                            �� I �           Icon Handle                       �c I1         Item Name                         �M � �          Item Index                         	                0    	            �    This function sets an attribute of a particular item on the specified System Tray icon's popup menu.

See the Class Help for more information on the recommended usage of System Tray icons and their popup menus.
    Q    The status returned by the function.  0 indicates success, a negative value indicates an error.  Call WT_GetErrorString () for a more detailed description of the error.

Possible values:

   0   WT_ERR_Success 
  -1   WT_ERR_NullPtr
  -2   WT_ERR_BufTooSmall       
  -3   WT_ERR_CantDynamicLink
  -4   WT_ERR_NoOSSupport
  -5   WT_ERR_OutOfMemory
  -6   WT_ERR_UnknownSysError
  -7   WT_ERR_InsufficientArgs
  -8   WT_ERR_InvalidAttrValue
  -9   WT_ERR_InvalidAttribute

  -50  WT_ERR_NoTrayIconMenu
  -51  WT_ERR_InvalidTrayMenuItem
  -52  WT_ERR_InvalidTrayIcon
  -53  WT_ERR_CantLoadIcon     �    The handle of the System Tray icon for which you wish to set a  popup menu item attribute.  This handle will have been returned from WT_InstallSysTrayIcon.     1    The desired value of the specified attribute.

          The attribute you wish to set.     �    The index of the item for which you wish to set an attribute.  The item index corresponds to its 1-based position on the popup menu, and will have been returned from WT_InsertTrayIconMenuItem ().    �X���         Status                            �� I �           Icon Handle                       �V �3          Value                             �� � �         Popup Menu Attribute              �� H+          Item Index                         	                               .Checked WT_ATTR_CHECKED Dimmed WT_ATTR_DIMMED    1    �    This function gets the value of an attribute of a particular item on the specified System Tray icon's popup menu.

See the Class Help for more information on the recommended usage of System Tray icons and their popup menus.
    Q    The status returned by the function.  0 indicates success, a negative value indicates an error.  Call WT_GetErrorString () for a more detailed description of the error.

Possible values:

   0   WT_ERR_Success 
  -1   WT_ERR_NullPtr
  -2   WT_ERR_BufTooSmall       
  -3   WT_ERR_CantDynamicLink
  -4   WT_ERR_NoOSSupport
  -5   WT_ERR_OutOfMemory
  -6   WT_ERR_UnknownSysError
  -7   WT_ERR_InsufficientArgs
  -8   WT_ERR_InvalidAttrValue
  -9   WT_ERR_InvalidAttribute

  -50  WT_ERR_NoTrayIconMenu
  -51  WT_ERR_InvalidTrayMenuItem
  -52  WT_ERR_InvalidTrayIcon
  -53  WT_ERR_CantLoadIcon     �    The handle of the System Tray icon for which you wish to get a  popup menu item attribute.  This handle will have been returned from WT_InstallSysTrayIcon.     2    The returned value of the specified attribute.

          The attribute you wish to get.     �    The index of the item for which you wish to get an attribute.  The item index corresponds to its 1-based position on the popup menu, and will have been returned from WT_InsertTrayIconMenuItem ().    �����         Status                            �& I �           Icon Handle                       �� �3          Value                             � � �         Popup Menu Attribute              �- H+          Item Index                         	                                0    1    E    This function removes a menu from the specified System Tray icon.

    Q    The status returned by the function.  0 indicates success, a negative value indicates an error.  Call WT_GetErrorString () for a more detailed description of the error.

Possible values:

   0   WT_ERR_Success 
  -1   WT_ERR_NullPtr
  -2   WT_ERR_BufTooSmall       
  -3   WT_ERR_CantDynamicLink
  -4   WT_ERR_NoOSSupport
  -5   WT_ERR_OutOfMemory
  -6   WT_ERR_UnknownSysError
  -7   WT_ERR_InsufficientArgs
  -8   WT_ERR_InvalidAttrValue
  -9   WT_ERR_InvalidAttribute

  -50  WT_ERR_NoTrayIconMenu
  -51  WT_ERR_InvalidTrayMenuItem
  -52  WT_ERR_InvalidTrayIcon
  -53  WT_ERR_CantLoadIcon     �    The handle of the System Tray icon for which you wish to detach a popup menu.  This handle will have been returned from WT_InstallSysTrayIcon ().    �{���         Status                            � I �           Icon Handle                        	                [    This function displays or copies a meaningful error description into the passed buffer.       ;    The error code returned from a WinTools library function.        If non-NULL, this buffer will be filled with a NULL-terminated ASCII description corresponding to the passed error code.  This buffer must be at least WT_VAL_MAX_ERROR bytes.

If this parameter is NULL, the function will display a run-time dialog with the error description.    L G �           Error Code                       � H9         Description                             ����         7  �     K.    RegWriteString                  ����         O  "m     K.    RegReadString                   ����         $w  +\     K.    RegWriteULong                   ����         -A  4B     K.    RegReadULong                    ����         6.  =�     K.    RegWriteBinary                  ����         ?�  I�     K.    RegReadBinary                   ����         K�  Q+     K.    GetOSVerInfo                    ����         Rp  U�     K.    GetWindowsDirs                  ����         Ve  Y/     K.    GetComputerName                 ����         Y�  ^<     K.    GetSystemColor                  ����         a�  f     K.    SetScreenSaverParams            ����         f�  jz     K.    GetScreenSaverParams            ����         k=  n�     K.    SetWallpaper                    ����         oH  s�     K.    GetWallpaper                    ����         ty  y�     K.    GetKeyboardParams               ����         {C  ~3     K.    SetKeyboardParams               ����         ~�  �     K.    GetDiskSpace                    ����         ��  ��     K.    GetMemorySpace                  ����         �  �     K.    InstallWinMsgCallback           ����         ��  �M     K.    SetMsgCallbackAttribute         ����         ��  ��     K.    GetMsgCallbackAttribute         ����         �*  �&     K.    RemoveWinMsgCallback            ����         ��  �v     K.    EnableDragAndDrop               ����         ��  �f     K.    DisableDragAndDrop              ����         ��  �_     K.    GetUserName                     ����         �\  ��     K.    InstallSysTrayIcon              ����         �(  ϓ     K.    RemoveSysTrayIcon               ����         �  �"     K.    SetTrayIconAttr                 ����         �  �`     K.    GetTrayIconAttr                 ����         �b  �k     K.    AttachTrayIconMenu              ����         ��  �     K.    SetTrayIconMenuAttr             ����         �  �X     K.    GetTrayIconMenuAttr             ����         �  ��     K.    InsertTrayIconMenuItem          ����         �{  ��     K.    SetTrayIconMenuItemAttr         ����         ��  ��     K.    GetTrayIconMenuItemAttr         ����         �. o     K.    DetachTrayIconMenu              ����        � �     K.    GetErrorString                         �                                    �Windows Registry                     DWrite String to Registry             DRead String from Registry            DWrite ULong to Registry              DRead ULong from Registry             DWrite Binary to Registry             DRead Binary from Registry           �System Configuration                 DGet OS Version                       DGet Windows Directories              DGet Computer Name                    GUI                                  DGet System Color                     DSet ScreenSaver Parameters           DGet ScreenSaver Parameters           DSet Desktop Wallpaper                DGet Desktop Wallpaper             ����Keyboard                             DGet Keyboard Parameters              DSet Keyboard Parameters             yResources                            DGet Disk Space                       DGet Memory Space                    �Windows Messaging                    DInstall Win Message Callback         DSet Callback Attribute               DGet Callback Attribute               DRemove Win Message Callback         	:Drag-And-Drop                        DEnable Drag-And-Drop                 DDisable Drag-And-Drop               
-User Information                     DGet User Name                       
�System Tray Icons                    DInstall System Tray Icon             DRemove System Tray Icon              DSet Tray Icon Attribute              DGet Tray Icon Attribute             :Popup Menus                          DAttach Tray Icon Popup Menu          DSet Popup Menu Attribute             DGet Popup Menu Attribute             DAdd Tray Icon Popup Menu Item        DSet Popup Menu Item Attribute        DGet Popup Menu Item Attribute        DDetach Tray Icon Popup Menu          DGet Error String                      toolbox.fp 