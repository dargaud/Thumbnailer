/////////////////////////////////////////////////////////////////////////////////////
// PROGRAM:   Thumbnailer.exe                                                      //
// COPYRIGHT: Guillaume Dargaud 2001 - Freeware                                    //
// PURPOSE:   Creation of thumbnail images for quick web browsing                  //
// INSTALL:   run SETUP. It will also install the LabWindows/CVI runtime engine.   //
// HELP:      available by right-clicking an item.                                 //
// TUTORIAL:  http://www.gdargaud.net/Hack/Thumbnailer.html                        //
/////////////////////////////////////////////////////////////////////////////////////

#define _VER_ "1.8"			// Version 1.8
#define _WEB_ "http://www.gdargaud.net/Hack/Thumbnailer.html"
#define _COPY_ "� 2001-2002 Guillaume Dargaud"

#include <formatio.h>
#include <ansi_c.h>
#include <utility.h>
#include <cvirte.h>		
#include <userint.h>
#include <iso646.h>

#include "toolbox.h"
#include "wintools.h"
#include "Def.h"
#include "StrfTime.h"

#include "GraphicFilter.h"
#include "FullConfirmPopup.h"
#include "FilterPopup.h"

#include "Thumbnailer.h"

static char Prefix[]="tn";		// This is prefixed to the images saved. Images starting with this are skipped

static int Pnl=0, ProgressHandle=0;
static int Aggreg=50;

static BOOL CreateLinks=FALSE, CreateDirLinks=FALSE, KeepStructure=FALSE;
static BOOL OverWriteAll=FALSE, NoToAll=FALSE;
static int MaxWidth=150, MaxHeight=150, MinWidth=100, MinHeight=100;
static int Org=0;
static unsigned char *DestBits=NULL;		// Destination bitmap of size MaxWidth, MaxHeight

static int NbConverted=0, NbToConvert=0, Filter=0;
static double SizeConverted=0, SizeToConvert=0;
static unsigned long NbFiles=0, NbCompleted=0;
static time_t StartTime;

static char DestFile[MAX_PATHNAME_LEN]="", 
			DestDir[MAX_DIRNAME_LEN]="", 
			SourceDir[MAX_PATHNAME_LEN]="", 
			URL[MAX_PATHNAME_LEN]="";
static FILE* DestinationFile=NULL;

static BOOL WriteFilename=TRUE, WriteDimensions=TRUE, WriteSize=TRUE, WriteCreation=FALSE, WriteModification=TRUE;

static void CheckRelation(const BOOL Ask);


///////////////////////////////////////////////////////////////////////////////
int main (int argc, char *argv[]) {
	int i, Size, pWidth=375, pTop=10, pLeft=10, menuBar;
	
	if (InitCVIRTE (0, argv, 0) == 0)
		return -1;	/* out of memory */

	if ((Pnl = LoadPanel (0, "Thumbnailer.uir", PNL)) < 0 ) {
		MessagePopup("Error", "Could not load user interface file Thumbnailer.uir.\n You may have to reinstall the program.");
		return -1;
	}

	SetCtrlAttribute(Pnl, PNL_SOURCE, ATTR_MAX_ENTRY_LENGTH, MAX_PATHNAME_LEN);
	SetCtrlAttribute(Pnl, PNL_DEST, ATTR_MAX_ENTRY_LENGTH, MAX_PATHNAME_LEN);
	SetCtrlAttribute(Pnl, PNL_RELATIVE, ATTR_MAX_ENTRY_LENGTH, MAX_PATHNAME_LEN);
	
	for (i=0; i<NbFilters5x5; i++) InsertListItem (Pnl, PNL_FILTER, -1, Filters5x5[i]->Name, i);
	
	WT_RegReadBinary (WT_KEY_HKCU, "SOFTWARE\\Dargaud Software\\Thumbnailer", "CreateLinks", (char*)&CreateLinks, sizeof(CreateLinks), &Size);
	WT_RegReadBinary (WT_KEY_HKCU, "SOFTWARE\\Dargaud Software\\Thumbnailer", "CreateDirLinks", (char*)&CreateDirLinks, sizeof(CreateDirLinks), &Size);
	WT_RegReadBinary (WT_KEY_HKCU, "SOFTWARE\\Dargaud Software\\Thumbnailer", "KeepStructure", (char*)&KeepStructure, sizeof(KeepStructure), &Size);
	WT_RegReadBinary (WT_KEY_HKCU, "SOFTWARE\\Dargaud Software\\Thumbnailer", "Org", (char*)&Org, sizeof(Org), &Size);
	WT_RegReadBinary (WT_KEY_HKCU, "SOFTWARE\\Dargaud Software\\Thumbnailer", "MaxWidth",  (char*)&MaxWidth,  sizeof(MaxWidth), &Size);
	WT_RegReadBinary (WT_KEY_HKCU, "SOFTWARE\\Dargaud Software\\Thumbnailer", "MaxHeight", (char*)&MaxHeight, sizeof(MaxHeight), &Size);
	WT_RegReadBinary (WT_KEY_HKCU, "SOFTWARE\\Dargaud Software\\Thumbnailer", "MinWidth",  (char*)&MinWidth,  sizeof(MinWidth), &Size);
	WT_RegReadBinary (WT_KEY_HKCU, "SOFTWARE\\Dargaud Software\\Thumbnailer", "MinHeight", (char*)&MinHeight, sizeof(MinHeight), &Size);
	WT_RegReadBinary (WT_KEY_HKCU, "SOFTWARE\\Dargaud Software\\Thumbnailer", "Filter", (char*)&Filter, sizeof(Filter), &Size);
	WT_RegReadBinary (WT_KEY_HKCU, "SOFTWARE\\Dargaud Software\\Thumbnailer", "SourceDir", (char*)&SourceDir, sizeof(SourceDir), &Size);
	WT_RegReadBinary (WT_KEY_HKCU, "SOFTWARE\\Dargaud Software\\Thumbnailer", "DestFile", (char*)&DestFile, sizeof(DestFile), &Size);
	WT_RegReadBinary (WT_KEY_HKCU, "SOFTWARE\\Dargaud Software\\Thumbnailer", "URL", (char*)&URL, sizeof(URL), &Size);
	WT_RegReadBinary (WT_KEY_HKCU, "SOFTWARE\\Dargaud Software\\Thumbnailer", "pWidth", (char*)&pWidth, sizeof(pWidth), &Size);
	WT_RegReadBinary (WT_KEY_HKCU, "SOFTWARE\\Dargaud Software\\Thumbnailer", "pLeft", (char*)&pLeft, sizeof(pLeft), &Size);
	WT_RegReadBinary (WT_KEY_HKCU, "SOFTWARE\\Dargaud Software\\Thumbnailer", "pTop", (char*)&pTop, sizeof(pTop), &Size);
	WT_RegReadBinary (WT_KEY_HKCU, "SOFTWARE\\Dargaud Software\\Thumbnailer", "WriteFilename", (char*)&WriteFilename, sizeof(WriteFilename), &Size);
	WT_RegReadBinary (WT_KEY_HKCU, "SOFTWARE\\Dargaud Software\\Thumbnailer", "WriteDimensions", (char*)&WriteDimensions, sizeof(WriteDimensions), &Size);
	WT_RegReadBinary (WT_KEY_HKCU, "SOFTWARE\\Dargaud Software\\Thumbnailer", "WriteSize", (char*)&WriteSize, sizeof(WriteSize), &Size);
//	WT_RegReadBinary (WT_KEY_HKCU, "SOFTWARE\\Dargaud Software\\Thumbnailer", "WriteCreation", (char*)&WriteCreation, sizeof(WriteCreation), &Size);
	WT_RegReadBinary (WT_KEY_HKCU, "SOFTWARE\\Dargaud Software\\Thumbnailer", "WriteModification", (char*)&WriteModification, sizeof(WriteModification), &Size);
	
	SetPanelSize (Pnl, 192, pWidth);
	SetPanelPos (Pnl, pTop, pLeft);
	
	SetCtrlAttribute(Pnl, PNL_PROCEED, ATTR_DIMMED, strlen(SourceDir)==0 or strlen(DestFile)==0);

	SetCtrlVal(Pnl, PNL_LINK, CreateLinks);
	SetCtrlVal(Pnl, PNL_DIR_LINK, CreateDirLinks);
	SetCtrlVal(Pnl, PNL_STRUCT, KeepStructure);
	SetCtrlVal(Pnl, PNL_ORG, Org);
	SetCtrlVal(Pnl, PNL_MAX_WIDTH, MaxWidth);
	SetCtrlVal(Pnl, PNL_MAX_HEIGHT, MaxHeight);
	SetCtrlVal(Pnl, PNL_MIN_WIDTH,  MinWidth);
	SetCtrlVal(Pnl, PNL_MIN_HEIGHT, MinHeight);
	SetCtrlVal(Pnl, PNL_FILTER, Filter);
	SetCtrlVal(Pnl, PNL_SOURCE, SourceDir);
	SetCtrlVal(Pnl, PNL_DEST, DestFile);
	SetCtrlVal(Pnl, PNL_RELATIVE, URL);

	SetCtrlAttribute(Pnl, PNL_RELATIVE, ATTR_DIMMED, ! CreateLinks and ! CreateDirLinks);
	
	menuBar = GetPanelMenuBar (Pnl);
	SetMenuBarAttribute (menuBar, MENU_EDIT_WRITE_FILENAME, ATTR_CHECKED, WriteFilename);
	SetMenuBarAttribute (menuBar, MENU_EDIT_WRITE_DIMENSIONS, ATTR_CHECKED, WriteDimensions);
	SetMenuBarAttribute (menuBar, MENU_EDIT_WRITE_SIZE, ATTR_CHECKED, WriteSize);
//	SetMenuBarAttribute (menuBar, MENU_EDIT_WRITE_CREATION, ATTR_CHECKED, WriteCreation);
	SetMenuBarAttribute (menuBar, MENU_EDIT_WRITE_MODIFICATION, ATTR_CHECKED, WriteModification);

	DisplayPanel (Pnl);
	
	
	RunUserInterface ();
	
	DiscardPanel (Pnl);
	
	return 0;
}



///////////////////////////////////////////////////////////////////////////////
// replace a character with another in a string
static char* ReplaceChar(char* Str, char Source, char Dest) {
	int St = SetBreakOnProtectionErrors (0);
	register char* Pos=Str-1;
	while (*++Pos!='\0') if (*Pos==Source) *Pos=Dest;
	SetBreakOnProtectionErrors(St);
	return Str;
}


///////////////////////////////////////////////////////////////////////////////
// Return the directory of a full path name (warning, uses a static buffer)
const char* DirectoryOfPath(const char* PathName) {
	static char Dir[MAX_PATHNAME_LEN]="", *P;
	P=strrchr(PathName, '\\');
	if (P==NULL) return strcpy(Dir, "\\");
	strncpy(Dir, PathName, P-PathName+1); Dir[P-PathName+1]='\0';
	return Dir;
}

///////////////////////////////////////////////////////////////////////////////
// Given a pathname (ignores the filename), will build the directories is they don't exist
// return TRUE if success
BOOL CreateDirStructure(const char* PathName) {
	char Dir[MAX_PATHNAME_LEN], Err[MAX_PATHNAME_LEN*2];
	int Res, i, ErrorLevel;
	
	strcpy(Dir, DirectoryOfPath(PathName));
	if (strlen(Dir)==0) return FALSE;
	if (strlen(Dir)==1 and Dir[0]=='\\') return TRUE;
	if (strlen(Dir)<3 and Dir[1]==':') return TRUE;
	if (Dir[strlen(Dir)-1]=='\\') Dir[strlen(Dir)-1]='\0';

	ErrorLevel=SetBreakOnLibraryErrors (0);
	Res = GetFileAttrs (Dir, &i, &i, &i, &i);
	SetBreakOnLibraryErrors (ErrorLevel);

	if (Res==1) return TRUE;		// Directory Found
	if (Res==0) ;	// File by that name, can we make a dir with same name ?
Again:
	ErrorLevel=SetBreakOnLibraryErrors (0);
	Res = MakeDir(Dir);
	SetBreakOnLibraryErrors (ErrorLevel);
	if (Res==0) return TRUE;
	if (Res==-1) {	
		Res=CreateDirStructure(Dir);	// Let's try to create one step up
		if (Res) goto Again;
	}
	sprintf(Err, "Error when making directory %s. Ignoring.", /* GetErrorCopy(Res),*/ Dir); 
	MessagePopup("Error", Err);
	return FALSE;		
}



static int CurrentTD=0;	// Column counter, if >0 then a table is open

#define IMG_PNG 1
#define IMG_JPEG 2
///////////////////////////////////////////////////////////////////////////////
// Reads an image (Png or Jpeg), resamples it and saves it at jpg
// Dir+Filename form the full path to the file to convert
// return TRUE if process need to abort
///////////////////////////////////////////////////////////////////////////////
BOOL ReadAndConvert(const char *Dir, const char *FileName, const int ImageType) {
	unsigned char *Bits=NULL, *DestPos=NULL, *SourcePos=NULL, *ROOF=NULL;
	char DestFileName[MAX_PATHNAME_LEN], 
		SourcePathName[MAX_PATHNAME_LEN]="", 
		RelPath[MAX_PATHNAME_LEN]="", 
		Str[MAX_PATHNAME_LEN+100],
		*Pos=NULL,
		StrTime[255],
		Comment[2*MAX_FILENAME_LEN]="";
	int RowBytes, PixDepth, Width, Height, BytesPerPixel;
	double Reduction=0;
	int DestWidth, DestHeight, RowBytesDest, PixDepthDest, BytesPerPixelDest,
		x, y, i, j, Res;
	unsigned long N, SR, SG, SB, MX, MY;
	long SourceSize, Size;
	BOOL OK=TRUE, Started=FALSE;
	static BOOL Side=FALSE;				// FALSE for image on left side
	time_t Now=time(NULL), TotalTime;
	long DiffTime, TotalDiffTime;
	
	if (0 == strncmp (Prefix, FileName, strlen(Prefix))) return FALSE;
	
	strcpy(SourcePathName, Dir);
	strcat(SourcePathName, FileName);
	
	SetCtrlVal(Pnl, PNL_FILE_NAME, FileName);
	//	printf("%s\n", FileName);

	DiffTime=Now-StartTime;
	TotalDiffTime=(DiffTime*NbFiles)/(NbCompleted+(NbCompleted==0?1:0));
	StrfTimeDiff (StrTime, 255, "Time remaining: %(days )D%(hours )h%(min )m%ssec ", TotalDiffTime, DiffTime);
	SetCtrlVal(Pnl, PNL_TIME_REMAIN, StrTime);

	if (GetFileInfo (SourcePathName, &SourceSize)!=1) return FALSE;

	int Bitmap=0, R=0;
	R=GetBitmapFromFile (SourcePathName, &Bitmap);
	if (R) return FALSE;	// TODO: better error handling
	R=GetBitmapDataEx (Bitmap, &RowBytes, &PixDepth, &Width, &Height, NULL, Bits, NULL, NULL);
	DiscardBitmap (Bitmap);
	if (R) return FALSE;	// TODO: better error handling
		
	if ((Width<MinWidth and Height<MinHeight) or PixDepth<8) goto TheEnd;

	BytesPerPixel=PixDepth/8;
	Reduction=Max( (double)Width/MaxWidth, (double)Height/MaxHeight);
	
	if (Reduction<=1) {		// The thumbnail is bigger than the image, let's keep the image size
		DestWidth=Width;
		DestHeight=Height;
		RowBytesDest=RowBytes;
		PixDepthDest=PixDepth;
		BytesPerPixelDest=BytesPerPixel;
		memcpy (DestBits, Bits, RowBytes*Height);
	} 
	else {				// Image needs resizing/resampling
		DestWidth=Width/Reduction;	 if (DestWidth==MaxWidth-1) DestWidth++;
		DestHeight=Height/Reduction; if (DestHeight==MaxHeight-1) DestHeight++;
	//	RowBytesDest=DestWidth*3;
		if (BytesPerPixel==1) {
			PixDepthDest=8;
			BytesPerPixelDest=1;
			RowBytesDest=DestWidth;	// Greyscale
			// RowBytesDest=((DestWidth+3)/4)*4;	// Greyscale
		} else {
			PixDepthDest=24;
			BytesPerPixelDest=3;
			RowBytesDest=((DestWidth*3+3)/4)*4;				// 24 bits (even if original 32)
		}
	
		ROOF=Bits+RowBytes*Height;		// Above maximum memory
		for (y=0; y<DestHeight; y++) {		// For each pixel of the destination image
			MY = MIN((y+1)*Reduction,Height);
			for (x=0; x<DestWidth; x++) {
				DestPos=&DestBits[x*BytesPerPixelDest + y*RowBytesDest];
				N=SR=SG=SB=0;
				MX = MIN((x+1)*Reduction,Width);
				for (j=y*Reduction; j<MY; j++)	{	// Resampling: 1 averaged block -> 1 pixel
					SourcePos=&Bits[ (int)(x*Reduction)*BytesPerPixel + j*RowBytes ];
					for (i=x*Reduction; i<MX; i++) {
						N++;
						/*if (SourcePos>=ROOF-1) continue; else*/ SR += *SourcePos++;
						if (BytesPerPixel==1 /*or SourcePos>=ROOF-3*/) continue;
						SG += *SourcePos++;
						SB += *SourcePos++;
						if (BytesPerPixel==4) SourcePos++;
					}
				}
				*DestPos++ = (unsigned char)(SR/N);
				if (BytesPerPixelDest==1) continue;
				*DestPos++ = (unsigned char)(SG/N);
				*DestPos   = (unsigned char)(SB/N);
			}			
		}
	}
	
	if (Filter!=0) 	// Do we want to apply the filter to non-resized images too ???
		Filter5x5(DestBits, RowBytesDest, PixDepthDest, DestWidth, DestHeight, Filters5x5[Filter]);
	
	// Create destination filename
	strcpy(DestFileName, DestDir);
	if (KeepStructure) 		// Relative PathName starts at &DestFileName[strlen(DestDir)])
		strcat(DestFileName, &Dir[strlen(SourceDir)]);
	strcat(DestFileName, Prefix);
	strcat(DestFileName, FileName);
	Pos=strrchr(DestFileName, '.');
	if (Pos!=NULL) 	strcpy(Pos, ".jpg");		// Change possible png to jpg
	else OK=FALSE;
	
	// Check for file presence and overwrite permission
	if (OK and !OverWriteAll and GetFileInfo (DestFileName, &Size)) {
		if (NoToAll) goto TheEnd;
		sprintf(Str, "File %s\nalready exists.\n\nDo you want to overwrite it ?", DestFileName);
		Res=FullConfirmPopup("File already exist", Str);
		switch (Res) {
			case VAL_POPUP_YES: 		OK=TRUE;				break;
			case VAL_POPUP_YES_TO_ALL:	OK=OverWriteAll=TRUE;  break;
			case VAL_POPUP_NO:			OK=FALSE;				break;
			case VAL_POPUP_NO_TO_ALL:	OK=FALSE; NoToAll=TRUE; break;
			case VAL_POPUP_CANCEL: 		OK=FALSE; DiscardProgressDialog(ProgressHandle); ProgressHandle=0; 
											if (Bits!=NULL) free(Bits); return TRUE;
		}
//		Res=GenericMessagePopup ("File already exist", Str, "__Yes", "Yes to __All", "__No", 
//							NULL, 0, 0, VAL_GENERIC_POPUP_BTN1,
//							VAL_GENERIC_POPUP_BTN1, VAL_GENERIC_POPUP_BTN3);
//		switch (Res) {
//			case VAL_GENERIC_POPUP_BTN1: OK=TRUE;				break;
//			case VAL_GENERIC_POPUP_BTN2: OK=OverWriteAll=TRUE;  break;
//			case VAL_GENERIC_POPUP_BTN3: OK=FALSE;				break;
//		}
	}
	
	if (OK) {
		
		if ((KeepStructure and !CreateDirStructure(DestFileName))) {
			sprintf(Str, "Problem saving thumbnail files %s", DestFileName);
			MessagePopup("Error", Str);
		} else {
			int Bitmap=0;
			NewBitmapEx (RowBytesDest, PixDepthDest, DestWidth, DestHeight, NULL, DestBits, NULL, NULL, &Bitmap);
			SetBitmapDataEx (Bitmap, RowBytesDest, PixDepthDest, NULL, DestBits, NULL, NULL);
			SaveBitmapToJPEGFile (Bitmap, DestFileName, 0, 80);
			DiscardBitmap (Bitmap); Bitmap=0;
		}
		Pos=&DestFileName[strlen(DestDir)];
		strcpy(RelPath, &SourcePathName[strlen(SourceDir)]);
		
		/////////////////////// Comment field //////////////////////
		#define STARTED { if (Started) strcat(Comment, " - "); Started=TRUE;}
		if (WriteFilename) { STARTED; sprintf(Comment, "%s", FileName); }
		if (WriteDimensions) { STARTED; sprintf(Comment, "%s%dx%d", Comment, Width, Height); }
		if (WriteSize) { STARTED; sprintf(Comment, "%s%dKb", Comment, SourceSize/1024); }
		if (WriteModification) { 
			int Year, Month, Day;
			GetFileDate (SourcePathName, &Month, &Day, &Year);
			STARTED; sprintf(Comment, "%s%d/%02d/%02d", Comment, Year, Month, Day); }
//		if (WriteCreation) { STARTED; sprintf(Comment, "%s%dx%d", Comment, Width, Height); }
		
		
		switch (Org) {
			case -1:  		// Left / Right
				if (CreateLinks) {
					ReplaceChar(RelPath, '\\', '/');		// replaces \ with /
					fprintf(DestinationFile, "<A HREF=\"%s%s\" TITLE=\"%s\" TARGET=_new>", 
							URL, RelPath, Comment);
				}
		
				fprintf(DestinationFile, "<IMG SRC=\"%s\" WIDTH=%d HEIGHT=%d ALT=\"%s\" CLASS=Pic%c>",
					ReplaceChar(Pos,'\\','/'), 
					DestWidth, DestHeight, 
					Comment, Side?'L':'R');
		
				if (CreateLinks) fprintf(DestinationFile, 
					"</A>\n<P CLASS=P%c><A HREF=\"%s%s\" TITLE=\"%s\" TARGET=_new>%s</A></P>\n", 
					Side?'L':'R', URL, RelPath, 
					Comment, Comment);
				else fprintf(DestinationFile, "\n<P CLASS=P%c>%s</P>\n", Side?'L':'R', Comment);

				Side=!Side;
				break;
			case 0:		// Stacked
				if (CreateLinks) {
					ReplaceChar(RelPath, '\\', '/');		// replaces \ with /
					fprintf(DestinationFile, "<SPAN><A HREF=\"%s%s\" TITLE=\"%s\" TARGET=_new>", 
							URL, RelPath, Comment);
				}
				else fprintf(DestinationFile, "<SPAN>");
		
				fprintf(DestinationFile, "<IMG SRC=\"%s\" WIDTH=%d HEIGHT=%d ALT=\"%s\">",
					ReplaceChar(Pos,'\\','/'), DestWidth, DestHeight, Comment);
		
				if (CreateLinks) fprintf(DestinationFile, "</A></SPAN>\n");
				else fprintf(DestinationFile, "</SPAN>\n");
				break;
			default:	// columns
				if (CurrentTD==0) fprintf(DestinationFile, "<TABLE WIDTH=%d>\n", Org*MaxWidth);
				if (CurrentTD % Org == 0) fprintf(DestinationFile, "<TR>\n");
				if (CurrentTD<Org) fprintf(DestinationFile, "<TD WIDTH=%d><CENTER>\n", MaxWidth);
				else fprintf(DestinationFile, "<TD><CENTER>\n");
				
				if (CreateLinks) {
					ReplaceChar(RelPath, '\\', '/');		// replaces \ with /
					fprintf(DestinationFile, "<A HREF=\"%s%s\" TITLE=\"%s\" TARGET=_new>", 
							URL, RelPath, Comment);
				}
		
				fprintf(DestinationFile, "<IMG SRC=\"%s\" WIDTH=%d HEIGHT=%d ALT=\"%s\">",
					ReplaceChar(Pos,'\\','/'), 
					DestWidth, DestHeight, Comment);
		
				if (CreateLinks) fprintf(DestinationFile, 
					"</A>\n<BR><A HREF=\"%s%s\" TITLE=\"%s\" TARGET=_new>%s</A></P>\n", 
					URL, RelPath, Comment, Comment);
				else fprintf(DestinationFile, "\n<BR>%s\n", Comment);

				fprintf(DestinationFile, "</CENTER></TD>\n");
				if ((++CurrentTD) % Org == 0) fprintf(DestinationFile, "</TR>\n");
		}
	}

TheEnd:
	if (Bits!=NULL) free(Bits);
	return FALSE;
}



///////////////////////////////////////////////////////////////////////////////
// Recursively converts all the files in a directory
// Skip the DestDir directory
// return TRUE if process needs to abort
///////////////////////////////////////////////////////////////////////////////
BOOL UpdateFileList(char* Dir, const BOOL CountOnly) {
	int Res=0, Type=0, ARd=0, ASys=0, AHd=0, AAr=0, i;
	char FileName[MAX_FILENAME_LEN], 
		DirName[MAX_DIRNAME_LEN], 
		SearchFor[MAX_PATHNAME_LEN], 
		RelPath[MAX_PATHNAME_LEN]="", 
		RelPath2[MAX_PATHNAME_LEN]="", 
		**SubDir=NULL, *Pos;
	int NbSubDir=0, MaxSubDir=0;
	BOOL IsJpeg, IsPng, FirstFile=TRUE, Cancel=FALSE;
	
	if (!CountOnly and ProgressHandle==0) return TRUE;	// Aborted process
	
	if (Dir[strlen(Dir)-1]!='\\') strcat(Dir, "\\");
	
	if (CompareStrings (DestDir, 0, Dir, 0, 0) == 0) return FALSE;	// We don't want the destination dir 

	SetCtrlVal(Pnl, PNL_DIR_NAME, Dir);
	
	strcpy(SearchFor, Dir);
	strcat(SearchFor, "*.*");
	
	Res = GetFirstFile (SearchFor, 1, 1, 0, 0, 1, 1, FileName);	// File+Dir search
	while (Res==0) {
//		printf("%s\n", FileName);
//		Type = GetFileAttrs ("", &ARd, &ASys, &AHd, &AAr);	// bug
		strcpy(SearchFor, Dir);
		strcat(SearchFor, FileName);
		Type = GetFileAttrs (SearchFor, &ARd, &ASys, &AHd, &AAr);
		
		if (Type==0) {
			IsJpeg=CompareStrings (SearchFor, strlen(SearchFor)-4, ".jpg", 0, 0)==0 or
					CompareStrings (SearchFor, strlen(SearchFor)-5, ".jpeg", 0, 0)==0;
			IsPng= CompareStrings (SearchFor, strlen(SearchFor)-4, ".png", 0, 0)==0;
			if (IsJpeg or IsPng) {
				if (CountOnly) SetCtrlVal(Pnl, PNL_NB_FILES, ++NbFiles);
				else {
					if (FirstFile) {
						if (CurrentTD>0) { 
							if (CurrentTD % Org != 0) fprintf(DestinationFile, "</TR>\n");
							CurrentTD=0; fprintf(DestinationFile, "</TABLE>\n"); 
						}
						Pos=&Dir[strlen(SourceDir)]-2;
						while (*Pos!=':' and *Pos!='\\') Pos--;
						Pos++;
						strcpy(RelPath, Pos);
						if (CreateDirLinks) {
							strcpy(RelPath2, &Dir[strlen(SourceDir)]);
							ReplaceChar(RelPath, '\\', '/');		// replaces \ with /
							ReplaceChar(RelPath2, '\\', '/');
							fprintf(DestinationFile, "\n<HR>\n<H2>Directory <A HREF=\"%s%s\">%s</A></H2>\n", 
									URL, RelPath2, RelPath);
						}
						else fprintf(DestinationFile, "\n<HR>\n<H2>Directory %s</H2>\n", RelPath);
						FirstFile=FALSE;
					}
					
					SetCtrlVal(Pnl, PNL_NB_FILES, NbFiles - NbCompleted++);
					if (Cancel=UpdateProgressDialog (ProgressHandle, NbCompleted*100/NbFiles, 1)) {
						// Abort operations
						DiscardProgressDialog (ProgressHandle);
						ProgressHandle=0;
						goto Abort;
					}
					else if (IsJpeg) {
							if (Cancel=ReadAndConvert(Dir, FileName, IMG_JPEG)) { 
								DiscardProgressDialog (ProgressHandle); ProgressHandle=0; goto Abort; 
							}
						}
					else if (IsPng)  {
							if (Cancel=ReadAndConvert(Dir, FileName, IMG_PNG )) { 
								DiscardProgressDialog (ProgressHandle); ProgressHandle=0; goto Abort; 
							}
						}
				}
			}
		} else if (Type==1) {	// Directory
			if (SubDir==NULL) SubDir=Calloc(MaxSubDir=Aggreg, char*);
			if (NbSubDir>=MaxSubDir) SubDir = (char**)realloc(SubDir, sizeof(char*)*(MaxSubDir+=Aggreg));
			SubDir[NbSubDir]=Calloc(MAX_DIRNAME_LEN, char);
			
			strcpy(SubDir[NbSubDir], Dir);
			strcat(SubDir[NbSubDir], FileName);
			
			NbSubDir++;
		}

		Res=GetNextFile(FileName);
	}

Abort:
	for (i=0; i<NbSubDir; i++) {
		// Loop on the subdirectories
		if (!Cancel) Cancel=UpdateFileList(SubDir[i], CountOnly);
		if (SubDir[i]!=NULL) free(SubDir[i]);
	}
	if (SubDir!=NULL) free(SubDir);

	if (CurrentTD>0) { 
		if (CurrentTD % Org != 0) fprintf(DestinationFile, "</TR>\n");
		CurrentTD=0; fprintf(DestinationFile, "</TABLE>\n");
	}
	return Cancel;
}




///////////////////////////////////////////////////////////////////////////////
int CVICALLBACK cb_Proceed (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	char *Pos=NULL, Buffer[255], Str[MAX_PATHNAME_LEN+50];
	FILE *FootFile, *HeadFile;

	switch (event) {
		case EVENT_COMMIT:
			SetWaitCursor (1);
			GetCtrlVal(Pnl, PNL_LINK, &CreateLinks);
			GetCtrlVal(Pnl, PNL_DIR_LINK, &CreateDirLinks);
			GetCtrlVal(Pnl, PNL_STRUCT, &KeepStructure);
			GetCtrlVal(Pnl, PNL_ORG, &Org);
			GetCtrlVal(Pnl, PNL_MIN_WIDTH, &MinWidth);
			GetCtrlVal(Pnl, PNL_MIN_HEIGHT, &MinHeight);
			GetCtrlVal(Pnl, PNL_MAX_WIDTH, &MaxWidth);
			GetCtrlVal(Pnl, PNL_MAX_HEIGHT, &MaxHeight);
			GetCtrlVal(Pnl, PNL_FILTER, &Filter);
		
			WT_RegWriteBinary(WT_KEY_HKCU, "SOFTWARE\\Dargaud Software\\Thumbnailer", "CreateLinks", (char*)&CreateLinks, sizeof(CreateLinks));
			WT_RegWriteBinary(WT_KEY_HKCU, "SOFTWARE\\Dargaud Software\\Thumbnailer", "CreateDirLinks", (char*)&CreateDirLinks, sizeof(CreateDirLinks));
			WT_RegWriteBinary(WT_KEY_HKCU, "SOFTWARE\\Dargaud Software\\Thumbnailer", "KeepStructure", (char*)&KeepStructure, sizeof(KeepStructure));
			WT_RegWriteBinary(WT_KEY_HKCU, "SOFTWARE\\Dargaud Software\\Thumbnailer", "Org", (char*)&Org, sizeof(Org));
			WT_RegWriteBinary(WT_KEY_HKCU, "SOFTWARE\\Dargaud Software\\Thumbnailer", "MaxWidth",  (char*)&MaxWidth,  sizeof(MaxWidth));
			WT_RegWriteBinary(WT_KEY_HKCU, "SOFTWARE\\Dargaud Software\\Thumbnailer", "MaxHeight", (char*)&MaxHeight, sizeof(MaxHeight));
			WT_RegWriteBinary(WT_KEY_HKCU, "SOFTWARE\\Dargaud Software\\Thumbnailer", "MinWidth",  (char*)&MinWidth,  sizeof(MinWidth));
			WT_RegWriteBinary(WT_KEY_HKCU, "SOFTWARE\\Dargaud Software\\Thumbnailer", "MinHeight", (char*)&MinHeight, sizeof(MinHeight));
			WT_RegWriteBinary(WT_KEY_HKCU, "SOFTWARE\\Dargaud Software\\Thumbnailer", "Filter", (char*)&Filter, sizeof(Filter));
			WT_RegWriteBinary(WT_KEY_HKCU, "SOFTWARE\\Dargaud Software\\Thumbnailer", "WriteFilename", (char*)&WriteFilename, sizeof(WriteFilename));
			WT_RegWriteBinary(WT_KEY_HKCU, "SOFTWARE\\Dargaud Software\\Thumbnailer", "WriteDimensions", (char*)&WriteDimensions, sizeof(WriteDimensions));
			WT_RegWriteBinary(WT_KEY_HKCU, "SOFTWARE\\Dargaud Software\\Thumbnailer", "WriteSize", (char*)&WriteSize, sizeof(WriteSize));
//			WT_RegWriteBinary(WT_KEY_HKCU, "SOFTWARE\\Dargaud Software\\Thumbnailer", "WriteCreation", (char*)&WriteCreation, sizeof(WriteCreation));
			WT_RegWriteBinary(WT_KEY_HKCU, "SOFTWARE\\Dargaud Software\\Thumbnailer", "WriteModification", (char*)&WriteModification, sizeof(WriteModification));
			

			// Source Dir
			GetCtrlVal(Pnl, PNL_SOURCE, SourceDir);
			if (SourceDir[0]=='\0') return 0;
			WT_RegWriteBinary(WT_KEY_HKCU, "SOFTWARE\\Dargaud Software\\Thumbnailer", "SourceDir", SourceDir, strlen(SourceDir)+1 /*MAX_PATHNAME_LEN*/);

			// Destination dir and file
			GetCtrlVal(Pnl, PNL_DEST, DestFile);
			if (DestFile[0]=='\0') return 0;
			WT_RegWriteBinary(WT_KEY_HKCU, "SOFTWARE\\Dargaud Software\\Thumbnailer", "DestFile", DestFile, strlen(DestFile)+1 /*MAX_PATHNAME_LEN*/);

			// URL Server path
			GetCtrlVal(Pnl, PNL_RELATIVE, URL);
			if (URL[0]=='\0') strcat(URL, "/");
			if (URL[strlen(URL)-1]!='/') strcat(URL, "/");
			SetCtrlVal(Pnl, PNL_RELATIVE, URL);
			WT_RegWriteBinary(WT_KEY_HKCU, "SOFTWARE\\Dargaud Software\\Thumbnailer", "URL", URL, strlen(URL)+1 /*MAX_PATHNAME_LEN*/);
			
			
			CheckRelation(FALSE);


			if (DestBits!=NULL) free(DestBits);
//			DestBits=Calloc( MaxWidth*3*MaxHeight, unsigned char);				// RGB image
			DestBits=Calloc( ((MaxWidth*3+3)/4)*4*(MaxHeight+1), unsigned char);	// RGB image, uses RowBytes alignement on 4. The +1 is a margin security
			if (DestBits==NULL) {
				SetWaitCursor (0);
				MessagePopup("Error", "Out of Memory !");
				exit(1);
			}

			
			OverWriteAll=NoToAll=FALSE;		// Default is no overwrite
			SetCtrlVal(Pnl, PNL_NB_FILES, NbFiles=NbCompleted=0);
			
			// Now let's create the destination file
			DestinationFile=fopen (DestFile, "w");
			if (DestinationFile==NULL) {
				SetWaitCursor (0);
				MessagePopup("Error", "Could not create destination file !");
				return 1;
			}
			
			// Copy the header into it
			HeadFile=fopen ("Head.html", "r");
			if (HeadFile==NULL) {
				SetWaitCursor (0);
				MessagePopup("Error", "File \"Head.html\" not found,\nyou might need to reinstall Thumbnailer.");
				exit(1);
			}
			while (fgets(Buffer, 255, HeadFile)!=NULL)
				if (Buffer[0]=='W' and Buffer[1]=='H')
					fprintf(DestinationFile, "\t\twidth:%d; height:%d;\n", MaxWidth, MaxHeight);
				else fputs (Buffer, DestinationFile);
			fclose(HeadFile);
				
			
			fprintf(DestinationFile, "<H1>Thumbnails for directory %s</H1>", SourceDir);

			// Some runtime indicators
			SetCtrlAttribute(Pnl, PNL_NB_FILES, ATTR_VISIBLE, TRUE);
			SetCtrlAttribute(Pnl, PNL_DIR_NAME, ATTR_VISIBLE, TRUE);
			SetCtrlAttribute(Pnl, PNL_FILE_NAME, ATTR_VISIBLE, TRUE);
			SetCtrlAttribute(Pnl, PNL_TIME_REMAIN, ATTR_VISIBLE, TRUE);
			SetCtrlAttribute (Pnl, PNL_NB_FILES, ATTR_LABEL_TEXT, "Images found");

			SetCtrlAttribute(Pnl, PNL_PROCEED, ATTR_VISIBLE, FALSE);

			///////////////////////////////////////////////////////////////////
			// Count the files
			UpdateFileList(SourceDir, TRUE);

			SetCtrlAttribute (Pnl, PNL_NB_FILES, ATTR_LABEL_TEXT, "Images remaining");
			StartTime=time(NULL);

			// Create the thumbnails and update each line with IMG links
			if (NbFiles>0) {
				ProgressHandle = CreateProgressDialog ("Creating thumbnails...", "Percent Complete", 1, VAL_FULL_MARKERS, "__Abort");
				UpdateFileList(SourceDir, FALSE);
				if (ProgressHandle!=0) DiscardProgressDialog (ProgressHandle);
			}
			
			// Remove the indicators
			SetCtrlAttribute(Pnl, PNL_NB_FILES, ATTR_VISIBLE, FALSE);
			SetCtrlAttribute(Pnl, PNL_DIR_NAME, ATTR_VISIBLE, FALSE);
			SetCtrlAttribute(Pnl, PNL_FILE_NAME, ATTR_VISIBLE, FALSE);
			SetCtrlAttribute(Pnl, PNL_TIME_REMAIN, ATTR_VISIBLE, FALSE);

			SetCtrlAttribute(Pnl, PNL_PROCEED, ATTR_VISIBLE, TRUE);

			///////////////////////////////////////////////////////////////////
			
			fprintf(DestinationFile, "<HR>\n<P>Thumbnails generated by <A HREF=\""_WEB_"\">Thumbnailer</A>, freeware.</P>");

			// Copy the footer into it
			FootFile=fopen ("Foot.html", "r");
			if (FootFile==NULL) {
				MessagePopup("Error", "File \"Foot.html\" not found,\nyou might need to reinstall Thumbnailer.");
			} else {
				while (fgets(Buffer, 255, FootFile)!=NULL)
					fputs (Buffer, DestinationFile);
				fclose(FootFile);
			}
			
			fclose(DestinationFile);

			sprintf(Str, "EXPLORER.EXE file://%s", DestFile);
			LaunchExecutable (Str);
			SetWaitCursor (0);
			break;

		
		
		case EVENT_RIGHT_CLICK:
			MessagePopup("Help: Create thumbnails", 
						"Explores the Source directory and convertes all jpg and png files to\n"
						"thumbnail files saved in the destination directory."); break;
	}
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
static void CheckRelation(const BOOL Ask) {
	char *DiffSource=SourceDir, *DiffDest=DestDir, *Pos, 
			RelPath[MAX_PATHNAME_LEN]="", Str[150+MAX_PATHNAME_LEN]="";
	
	if (strlen(SourceDir)>0 and SourceDir[strlen(SourceDir)-1]!='\\') strcat(SourceDir, "\\");
	SetCtrlVal(Pnl, PNL_SOURCE, SourceDir);

	Pos=strrchr(DestFile, '\\');
	if (Pos!=NULL) strncpy(DestDir, DestFile, Pos-DestFile+1), DestDir[Pos-DestFile+1]='\0'; 
	else DestDir[0]='\0';

	if (!(CreateLinks or CreateDirLinks) or SourceDir[0]=='\0' or DestDir[0]=='\0' or !Ask) return;

	while (toupper(*DiffSource)==toupper(*DiffDest)) DiffSource++, DiffDest++;
	
	if (DiffSource==SourceDir) return;	// Different disk or machine
	
	while (*DiffDest!='\0') if (*DiffDest++=='\\') strcat(RelPath, "../");
	
	strcat(RelPath, DiffSource);
	
	Pos=RelPath;
	while ((Pos=strchr(Pos, '\\'))!=NULL) *Pos='/';
	
	if (CompareStrings (RelPath, 0, URL, 0, 0)!=0) {
		sprintf(Str, "Source and destination have a common part,\n"
				"do you want to use \"%s\" as a relative URL ?", RelPath);

		if (ConfirmPopup ("Set relative URL ?", Str))
			SetCtrlVal(Pnl, PNL_RELATIVE, strcpy(URL, RelPath));
	}
}

///////////////////////////////////////////////////////////////////////////////
int CVICALLBACK cb_InputSource (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT:
			GetCtrlVal(Pnl, PNL_SOURCE, SourceDir);
			SetCtrlAttribute(Pnl, PNL_PROCEED, ATTR_DIMMED, strlen(SourceDir)==0 or strlen(DestFile)==0);
			CheckRelation(TRUE);
			break;
		case EVENT_LEFT_DOUBLE_CLICK:
			if (VAL_DIRECTORY_SELECTED!=DirSelectPopup (SourceDir, "Select Directory of files to convert", 1, 0, SourceDir))
				break;
			if (strlen(SourceDir)>0 and SourceDir[strlen(SourceDir)-1]!='\\') strcat(SourceDir, "\\");
			SetCtrlVal(Pnl, PNL_SOURCE, SourceDir);
			SetCtrlAttribute(Pnl, PNL_PROCEED, ATTR_DIMMED, strlen(SourceDir)==0 or strlen(DestFile)==0);
			CheckRelation(TRUE);
			break;
		case EVENT_RIGHT_CLICK:
			MessagePopup("Help: Source Directory", 
						"This is the directory of the files to convert.\n"
						"All subdirectories are exploredand all Jpg and Png files above the minimal size are converted.\n"
						"\nDouble-click to browse."); break;
	}
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
int CVICALLBACK cd_InputDest (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT:
			GetCtrlVal(Pnl, PNL_DEST, DestFile);
			SetCtrlAttribute(Pnl, PNL_PROCEED, ATTR_DIMMED, strlen(SourceDir)==0 or strlen(DestFile)==0);
			CheckRelation(TRUE);
			break;
		case EVENT_LEFT_DOUBLE_CLICK:
			if (VAL_NO_FILE_SELECTED>=FileSelectPopup ("SourceDir", "*.html", "*.html;*.jpg;*.png",
							 "Select destination HTML file", VAL_SAVE_BUTTON, 0,
							 1, 1, 1, DestFile))
				break;
			SetCtrlVal(Pnl, PNL_DEST, DestFile);
			SetCtrlAttribute(Pnl, PNL_PROCEED, ATTR_DIMMED, strlen(SourceDir)==0 or strlen(DestFile)==0);
			CheckRelation(TRUE);
			break;
		case EVENT_RIGHT_CLICK:
			MessagePopup("Help: Destination file", 
						"This is the destination HTML file to create.\n"
						"All thumbnails will be created in that same directory.\n"
						"\nDouble-click to browse."); break;
	}
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
int CVICALLBACK cb_Help (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	int Res;
	switch (event) {
		case EVENT_RIGHT_CLICK:
		case EVENT_LEFT_DOUBLE_CLICK:
			Res = LaunchExecutable ("EXPLORER.EXE http://www.gdargaud.net/Hack/Thumbnailer.html");
			//if (Res!=0) MessagePopup("Error!",GetUILErrorString (Status));
			break;
	}
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
int CVICALLBACK cb_Quit (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT:
			QuitUserInterface (0);
			break;
	}
	return 0;
}

int CVICALLBACK cb_Struct (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_RIGHT_CLICK:
			MessagePopup("Help: Keep structure", 
						"Saves the thumbnail images in a similar directory structure than the original files, below the destination folder.\n"
						"Otherwise saves them all in the destination folder.\n"
						"\nNote that they can overwrite each other if they have the same names."); 
			break;
	}
	return 0;
}

int CVICALLBACK cb_Org (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_RIGHT_CLICK:
			MessagePopup("Organisation of the images", 
						"[Stacked] will organize the thumbnails next to each other,\n"
						"  Using as little room as possible\n"
						"  (works best for tiny thumbnails without text).\n"
						"[Left-Right] will place one image on the left, one on the right,\n"
						"  then one on the left below the first one...\n"
						"  (works best for bigger sizes with text)\n"
						"[Columns] makes a table of the specified number of columns.\n"); 
			break;
	}
	return 0;
}

int CVICALLBACK cb_Link (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT:
			GetCtrlVal(Pnl, PNL_LINK, &CreateLinks);
			SetCtrlAttribute(Pnl, PNL_RELATIVE, ATTR_DIMMED, ! CreateLinks and ! CreateDirLinks);
			break;
		case EVENT_RIGHT_CLICK:
			MessagePopup("Help: Create image links", 
						"Create links on each thumbnail, pointing to the original image.\n"
						"For this to work on a server, you probably need to edit the HTML file created."); 
			break;
	}
	return 0;
}

int CVICALLBACK cb_DirLink (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT:
			GetCtrlVal(Pnl, PNL_DIR_LINK, &CreateDirLinks);
			SetCtrlAttribute(Pnl, PNL_RELATIVE, ATTR_DIMMED, ! CreateLinks and ! CreateDirLinks);
			break;
		case EVENT_RIGHT_CLICK:
			MessagePopup("Help: Create directory links", 
						"Create links on each directory, pointing to the original directory.\n"
						"For this to work on a server, you probably need to edit the HTML file created."); 
			break;
	}
	return 0;
}

int CVICALLBACK cb_MaxHeightWidth (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_RIGHT_CLICK:
			MessagePopup("Help: Maximum Width/Height", 
						"Maximum width/height of the thumbnail to create.\n"
						"Note that files smaller than this will keep their original size."); 
			break;
	}
	return 0;
}

int CVICALLBACK cb_MinHeightWidth (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_RIGHT_CLICK:
			MessagePopup("Help: Minimum Width/Height", 
						"Minimum size of the files to convert.\n"
						"Anything smaller will be ignored."); 
			break;
	}
	return 0;
}

int CVICALLBACK cd_Relative (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_RIGHT_CLICK:
			MessagePopup("Help: URL server path", 
						"If using links to the real images, use this as a full or relative path.\n"
						"For instance, if the images you want to thumbnail are in C:\\Perso\\WebFolder\\Images\n"
						"Wich is being served as http://www.mysite.com/johndoe/Images/\n"
						"And you are saving the tumbnail file in C:\\Perso\\WebFolder\\Thumbnails\\Thumb.html\n"
						"Then you might write either http://www.mysite.com/johndoe/Images/\n"
						"or ../Images/ as path to the large images."); 
			break;
	}
	return 0;
}

int CVICALLBACK cb_NbFiles (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_RIGHT_CLICK:
			MessagePopup("Help: Nb Files", 
						"Number of files to convert or remaining to convert."); 
			break;
	}
	return 0;
}

int CVICALLBACK cb_Filter (int panel, int control, int event,
		void *callbackData, int eventData1, int eventData2) {
	switch (event) {
		case EVENT_COMMIT:
			GetCtrlVal(Pnl, PNL_FILTER, &Filter);
			if (Filters5x5[Filter]==&FilterCustom5x5) Filter5x5Popup(&FilterCustom5x5);
			break;
		case EVENT_RIGHT_CLICK:
			MessagePopup("Help: Use Filter", 
						"Select a graphic filter to use on the resulting thumbnail.\n"
						"Filters can enhance the image by sharpening it or other."); 
			break;
	}
	return 0;
}

int CVICALLBACK cbp_Panel (int panel, int event, void *callbackData,
		int eventData1, int eventData2) {
	int pWidth, Left, Top, Width;
	switch (event) {
		case EVENT_PANEL_SIZE:
			GetPanelAttribute (Pnl, ATTR_WIDTH, &pWidth);
			SetPanelAttribute (Pnl, ATTR_HEIGHT, 192);
			if (pWidth<375) SetPanelAttribute (Pnl, ATTR_WIDTH, pWidth=375);
			GetCtrlAttribute(Pnl, PNL_SOURCE,  ATTR_LEFT, &Left);
			SetCtrlAttribute(Pnl, PNL_SOURCE,  ATTR_WIDTH, pWidth-Left-1);
			SetCtrlAttribute(Pnl, PNL_DEST,    ATTR_WIDTH, pWidth-Left-1);
			SetCtrlAttribute(Pnl, PNL_RELATIVE,ATTR_WIDTH, pWidth-Left-1);
			
//			GetCtrlAttribute(Pnl, PNL_PROCEED, ATTR_WIDTH, &Width);
//			SetCtrlAttribute(Pnl, PNL_PROCEED, ATTR_LEFT, pWidth-Width-1);

			WT_RegWriteBinary(WT_KEY_HKCU, "SOFTWARE\\Dargaud Software\\Thumbnailer", "pWidth", (char*)&pWidth, sizeof(pWidth));
			break;

		case EVENT_PANEL_MOVE:
			GetPanelAttribute (Pnl, ATTR_LEFT, &Left);
			GetPanelAttribute (Pnl, ATTR_TOP,  &Top);
			WT_RegWriteBinary(WT_KEY_HKCU, "SOFTWARE\\Dargaud Software\\Thumbnailer", "pLeft", (char*)&Left, sizeof(Left));
			WT_RegWriteBinary(WT_KEY_HKCU, "SOFTWARE\\Dargaud Software\\Thumbnailer", "pTop",  (char*)&Top,  sizeof(Top));
			break;
	}
	return 0;
}

///////////////////////////////////////////////////////////////////////////////
void CVICALLBACK cbm_QuickHelp (int menuBar, int menuItem, void *callbackData, int panel) {
	MessagePopup("Thumbnailer Quick Help", 
				"This program allows you to quickly create an html page of thumbnail images.\n"
				"1- Select a [Source Directory] containing JPG or PNG images\n"
				"2- Select a [Destination HTML] file to write (the thumbnails will be written there as well)\n"
				"3- Select options like filter, compression level, wether you want links or not\n"
				"4- Press Thumbnails and wait till the page displays\n"
				"\n"
				"You can edit the resulting page.");
}

void CVICALLBACK cbm_Website (int menuBar, int menuItem, void *callbackData, int panel) {
	LaunchExecutable ("EXPLORER.EXE "_WEB_);
}

void CVICALLBACK cbm_About (int menuBar, int menuItem, void *callbackData, int panel) {
	MessagePopup("About Thumbnailer", 
				"Thumbnailer.exe version " _VER_ "\n"
				"Freeware "_COPY_"\n"
				"Last compiled on " __DATE__ "\n"
				"More information and tutorial at "_WEB_);
}


void CVICALLBACK cbm_Quit (int menuBar, int menuItem, void *callbackData, int panel) {
	QuitUserInterface(0);
}

void CVICALLBACK cbm_SourceDir (int menuBar, int menuItem, void *callbackData, int panel) {
	cb_InputSource (Pnl, PNL_SOURCE, EVENT_LEFT_DOUBLE_CLICK, NULL, 0, 0);
}

void CVICALLBACK cbm_DestHtml(int menuBar, int menuItem, void *callbackData, int panel) {
	cd_InputDest (Pnl, PNL_DEST, EVENT_LEFT_DOUBLE_CLICK, NULL, 0, 0);
}

void CVICALLBACK cbm_EditHeader(int menuBar, int menuItem, void *callbackData, int panel) {
	char Path[MAX_PATHNAME_LEN], Cmd[MAX_PATHNAME_LEN+20]="Notepad ";
	if (0==GetFullPathFromProject ("Head.html", Path))
		LaunchExecutable (strcat(Cmd, Path));
}

void CVICALLBACK cbm_EditFooter(int menuBar, int menuItem, void *callbackData, int panel) {
	char Path[MAX_PATHNAME_LEN], Cmd[MAX_PATHNAME_LEN+20]="Notepad ";
	if (0==GetFullPathFromProject ("Foot.html", Path))
		LaunchExecutable (strcat(Cmd, Path));
}

///////////////////////////////////////////////////////////////////////////////
void CVICALLBACK cbm_Write(int menuBar, int menuItem, void *callbackData, int panel) {
	int Checked;
	GetMenuBarAttribute (menuBar, menuItem, ATTR_CHECKED, &Checked);
	SetMenuBarAttribute (menuBar, menuItem, ATTR_CHECKED, !Checked);
	switch (menuItem) {
		case MENU_EDIT_WRITE_FILENAME        :WriteFilename=!Checked; break;
		case MENU_EDIT_WRITE_DIMENSIONS      :WriteDimensions=!Checked; break;
		case MENU_EDIT_WRITE_SIZE            :WriteSize=!Checked; break;
//		case MENU_EDIT_WRITE_CREATION        :WriteCreation=!Checked; break;
		case MENU_EDIT_WRITE_MODIFICATION    :WriteModification=!Checked; break;
	}
}
