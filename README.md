# Thumbnailer

Obsolete LabWindows/CVI program to create thumbnails from a directory of images

PROGRAM:   Thumbnailer.exe  

VERSION:   1.9

COPYRIGHT: © 2001-2002 Guillaume Dargaud - Freeware - GPLv3

PURPOSE:   Convert PNG or JPEG files to thumbnails of a given dimension.
            Also creates a webpage that optionally links to them.

HELP:      available by right-clicking an item on the user interface.

TUTORIAL:  http://www.gdargaud.net/Hack/Thumbnailer.html
