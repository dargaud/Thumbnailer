/**************************************************************************************************
	FILE:       def.h

	PURPOSE:    General declarations and macros. Used by both MSVC and LabWindows/CVI
	NOTE:       The macros _NI_mswin_ and _MSC_VER controls whether to compile for CVI or MSVC
**************************************************************************************************/

#ifndef _DEF_H
#define _DEF_H

#include <iso646.h>	// for 'and', 'or'...

#ifndef NULL
	#define NULL ((void*)0)
#endif

///////////////////////////////////////////////////////////////////////////////
// True IEEE Nan and Inf handling
#define dNaN  0xFFF8000000000000ULL
#define dpInf 0xFFF0000000000000ULL
#define dnInf 0x7FF0000000000000ULL
#define IsDoubleNaN(d)      (*(unsigned long long*)&(d)==dNaN)
#define IsDoublePlusInf(d)  (*(unsigned long long*)&(d)==dpInf)
#define IsDoubleMinusInf(d) (*(unsigned long long*)&(d)==dnInf)
#define IsDoubleInf(d) (IsDoublePlusInf(d) or IsDoubleMinusInf(d))
#define IsInvalid(d) (IsDoubleNaN(d) or IsDoublePlusInf(d) or IsDoubleMinusInf(d))


// Inplace Swap 32 bit endian value. Parameter assumed to be unsigned short/long/longlong
// It's faster to use system-level functions like bswap_64 from byteswap.h
#ifndef SWAP_ENDIAN_32
#define SWAP_ENDIAN_16(s) ( (((s) bitand 0x00FF) << 8) bitor\
							(((s) bitand 0xFF00) >> 8) )
#define SWAP_ENDIAN_32(u) ( (((u) bitand 0x000000FFUL) << (3*8)) bitor\
							(((u) bitand 0x0000FF00UL) <<    8 ) bitor\
							(((u) bitand 0x00FF0000UL) >>    8 ) bitor\
							(((u) bitand 0xFF000000UL) >> (3*8)) )
#define SWAP_ENDIAN_64(l) ( (((l) bitand 0x00000000000000FFULL) << (7*8)) bitor\
							(((l) bitand 0x000000000000FF00ULL) << (5*8)) bitor\
							(((l) bitand 0x0000000000FF0000ULL) << (3*8)) bitor\
							(((l) bitand 0x00000000FF000000ULL) <<    8 ) bitor\
							(((l) bitand 0x000000FF00000000ULL) >>    8 ) bitor\
							(((l) bitand 0x0000FF0000000000ULL) >> (3*8)) bitor\
							(((l) bitand 0x00FF000000000000ULL) >> (5*8)) bitor\
							(((l) bitand 0xFF00000000000000ULL) >> (7*8)) )
#endif


///////////////////////////////////////////////////////////////////////////////
// Does a bit mask from bit L to bit H inclusive on 64 bits.
// H must be 63>=H and H>=L
// Example: BM(7,4) -> 0xF0
#define BM(H,L) ( (2ULL<<(H))-(1ULL<<(L)) )
// Mask value W and shift by L bits: SM(0x1234,12,4)=0x23
#define  SM(W,H,L) (((W) & BM(H,L)) >> (L))
// This is technically valid if H-L<32
#define iSM(W,H,L) (unsigned int)SM(W,H,L)


///////////////////////////////////////////////////////////////////////////////
#define Pi       3.1415926535897932384626433832795028841971L
#define RAD(x)   ((x)*PI/180.0)
// Normalized sin(x)/x
#define SINC(x)  ((x) != 0.0  ? sin(PI*(x))/(PI*(x)) : 1.0)
#define Ln10     2.3025850929940456840179914546844L    // For inverse of log10. Use 10^x==exp(x*Ln10)
#define Exp10(x) exp((x)*Ln10)
// Use * for conversions between degrees and radians
#define DegToRad 0.017453292519943295769236907684886L 
#define RadToDeg 57.295779513082320876798154814105L   

///////////////////////////////////////////////////////////////////////////////
// Time conversion
#define MS2CVI_TIME   2208902400   // Because MS time starts at 1/1/1970 and ANSI C at 1/1/1900
//#define MS2CVI_TIME 2208978000   // Because MS time starts at 1/1/1970 and ANSI C at 1/1/1900
#define IsCviTime(t) ((t)>=MS2CVI_TIME)	// Check if a date after 1970 is in CVI format
#define XL2CVI_TIME(XL) (((XL) - 25569) * 86400)	// XL is a double, result is time_t
#define CVI2XL_TIME(t) ((t)/86400. + 25569)			// t is time_t, result is a double


///////////////////////////////////////////////////////////////////////////////
#define Malloc(type)   (type *)malloc(sizeof(type))
#define Calloc(n,type) (type *)calloc(n, sizeof(type))
#define Free(Ptr) { free(Ptr); Ptr=NULL; }

///////////////////////////////////////////////////////////////////////////////
// BEWARE OF SIDE EFFECTS. It'd be better to use a compound statement
#define MIN(a,b)    ((a)<=(b) ? (a):(b))
#define MAX(a,b)    ((a)>=(b) ? (a):(b))
#define MIN3(a,b,c) ((a)<=(b) ? (a)<=(c)?(a):(c) : (b)<=(c)?(b):(c) )
#define MAX3(a,b,c) ((a)>=(b) ? (a)>=(c)?(a):(c) : (b)>=(c)?(b):(c) )
#define BETWEEN(a,b,c) ((a)<=(b) and (b)<=(c))
#define FORCEPIN(a,b,c) ((b)<=(a) ? (a) : (c)<=(b) ? (c) : (b))	// Forces b to be between a and c
#define SIGN(a) ((a)>0 ? 1 : (a)<0 ? -1 : 0)

///////////////////////////////////////////////////////////////////////////////
// Log of 2 - related macros. Find closest 2^n
#define InvLn2 1.4426950408889634073599246810019L       // 1/ln(2)
#define FloorLog2(x) ((int)(floor(log(x)*InvLn2)))
#define RoundLog2(x) ((int)(floor(log(x)*InvLn2+0.5)))
#define CeilLog2(x)  ((int)(ceil( log(x)*InvLn2)))

///////////////////////////////////////////////////////////////////////////////
typedef struct {
	float Re, Im;
} complex;

//typedef enum {false, true} bool;

// Basic types (from Windows)
//#pragma warning(disable:4209 4142)

typedef unsigned char  BYTE;        // 8-bit unsigned entity
typedef unsigned short WORD;        // 16-bit unsigned number
typedef unsigned int   UINT;        // machine sized unsigned number (preferred)
//typedef long           LONG;      // 32-bit signed number
typedef unsigned long  DWORD;       // 32-bit unsigned number
typedef void*          POSITION;    // abstract iteration position

#ifndef BOOL
	#define BOOL int
#endif

#ifdef _CVI_
	#pragma iso_9899_1999		// Keep __FUNCTION__ as a macro
#endif


#ifndef TRUE
	#define TRUE 1
	#define FALSE 0
#endif
#ifndef YES
	#define YES 1
	#define NO 0
#endif
#ifndef OPEN
	#define OPEN 1
	#define CLOSE 0
#endif
#ifndef MODIF
	#define MODIF 1
	#define NOMODIF 0
#endif

///////////////////////////////////////////////////////////////////////////////
//#pragma warning(default:4209 4142)

#ifdef _DEBUG
	// NOTE: you should #include <stdlib.h> if you use those macros
	#define TRACE     printf
	#define ASSERT(f) ((f) ? (void)0 : printf("Assertion " #f " failed file %s line %d",__FILE__, __LINE__))
#else
	#define ASSERT(f) ((void)0)
	#define TRACE     ((void)0)
#endif // _DEBUG


#endif // _DEF
