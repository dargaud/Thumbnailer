/**************************************************************************/
/* LabWindows/CVI User Interface Resource (UIR) Include File              */
/*                                                                        */
/* WARNING: Do not add to, delete from, or otherwise modify the contents  */
/*          of this include file.                                         */
/**************************************************************************/

#include <userint.h>

#ifdef __cplusplus
    extern "C" {
#endif

     /* Panels and Controls: */

#define  PFAE                             1
#define  PFAE_CANCEL                      2       /* control type: command, callback function: cb_FullConfirm */
#define  PFAE_NO_ALL                      3       /* control type: command, callback function: cb_FullConfirm */
#define  PFAE_NO                          4       /* control type: command, callback function: cb_FullConfirm */
#define  PFAE_YES_ALL                     5       /* control type: command, callback function: cb_FullConfirm */
#define  PFAE_YES                         6       /* control type: command, callback function: cb_FullConfirm */
#define  PFAE_TEXTMSG                     7       /* control type: textMsg, callback function: (none) */

#define  PNL                              2       /* callback function: cbp_Panel */
#define  PNL_SOURCE                       2       /* control type: string, callback function: cb_InputSource */
#define  PNL_DEST                         3       /* control type: string, callback function: cd_InputDest */
#define  PNL_RELATIVE                     4       /* control type: string, callback function: cd_Relative */
#define  PNL_DIR_LINK                     5       /* control type: radioButton, callback function: cb_DirLink */
#define  PNL_LINK                         6       /* control type: radioButton, callback function: cb_Link */
#define  PNL_STRUCT                       7       /* control type: radioButton, callback function: cb_Struct */
#define  PNL_FILTER                       8       /* control type: ring, callback function: cb_Filter */
#define  PNL_MIN_WIDTH                    9       /* control type: numeric, callback function: cb_MinHeightWidth */
#define  PNL_MIN_HEIGHT                   10      /* control type: numeric, callback function: cb_MinHeightWidth */
#define  PNL_MAX_WIDTH                    11      /* control type: numeric, callback function: cb_MaxHeightWidth */
#define  PNL_MAX_HEIGHT                   12      /* control type: numeric, callback function: cb_MaxHeightWidth */
#define  PNL_NB_FILES                     13      /* control type: numeric, callback function: cb_NbFiles */
#define  PNL_PROCEED                      14      /* control type: command, callback function: cb_Proceed */
#define  PNL_QUIT                         15      /* control type: command, callback function: cb_Quit */
#define  PNL_ORG                          16      /* control type: ring, callback function: cb_Org */
#define  PNL_DIR_NAME                     17      /* control type: textMsg, callback function: (none) */
#define  PNL_FILE_NAME                    18      /* control type: textMsg, callback function: (none) */
#define  PNL_TIME_REMAIN                  19      /* control type: textMsg, callback function: (none) */
#define  PNL_TEXTMSG_4                    20      /* control type: textMsg, callback function: (none) */
#define  PNL_TEXTMSG_5                    21      /* control type: textMsg, callback function: (none) */


     /* Control Arrays: */

          /* (no control arrays in the resource file) */


     /* Menu Bars, Menus, and Menu Items: */

#define  MENU                             1
#define  MENU_FILE                        2
#define  MENU_FILE_SOURCE_DIR             3       /* callback function: cbm_SourceDir */
#define  MENU_FILE_DEST_HTML              4       /* callback function: cbm_DestHtml */
#define  MENU_FILE_SEPARATOR              5
#define  MENU_FILE_QUIT                   6       /* callback function: cbm_Quit */
#define  MENU_EDIT                        7
#define  MENU_EDIT_HEADER                 8       /* callback function: cbm_EditHeader */
#define  MENU_EDIT_FOOTER                 9       /* callback function: cbm_EditFooter */
#define  MENU_EDIT_SEPARATOR_2            10
#define  MENU_EDIT_WRITE_FILENAME         11      /* callback function: cbm_Write */
#define  MENU_EDIT_WRITE_DIMENSIONS       12      /* callback function: cbm_Write */
#define  MENU_EDIT_WRITE_SIZE             13      /* callback function: cbm_Write */
#define  MENU_EDIT_WRITE_CREATION         14      /* callback function: cbm_Write */
#define  MENU_EDIT_WRITE_MODIFICATION     15      /* callback function: cbm_Write */
#define  MENU_HELP                        16
#define  MENU_HELP_QUICK_HELP             17      /* callback function: cbm_QuickHelp */
#define  MENU_HELP_WEBSITE                18      /* callback function: cbm_Website */
#define  MENU_HELP_ABOUT                  19      /* callback function: cbm_About */


     /* Callback Prototypes: */

int  CVICALLBACK cb_DirLink(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_Filter(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_FullConfirm(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_InputSource(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_Link(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_MaxHeightWidth(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_MinHeightWidth(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_NbFiles(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_Org(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_Proceed(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_Quit(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cb_Struct(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
void CVICALLBACK cbm_About(int menubar, int menuItem, void *callbackData, int panel);
void CVICALLBACK cbm_DestHtml(int menubar, int menuItem, void *callbackData, int panel);
void CVICALLBACK cbm_EditFooter(int menubar, int menuItem, void *callbackData, int panel);
void CVICALLBACK cbm_EditHeader(int menubar, int menuItem, void *callbackData, int panel);
void CVICALLBACK cbm_QuickHelp(int menubar, int menuItem, void *callbackData, int panel);
void CVICALLBACK cbm_Quit(int menubar, int menuItem, void *callbackData, int panel);
void CVICALLBACK cbm_SourceDir(int menubar, int menuItem, void *callbackData, int panel);
void CVICALLBACK cbm_Website(int menubar, int menuItem, void *callbackData, int panel);
void CVICALLBACK cbm_Write(int menubar, int menuItem, void *callbackData, int panel);
int  CVICALLBACK cbp_Panel(int panel, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cd_InputDest(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);
int  CVICALLBACK cd_Relative(int panel, int control, int event, void *callbackData, int eventData1, int eventData2);


#ifdef __cplusplus
    }
#endif
